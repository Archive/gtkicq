#
# Configuration of the gnome-libs package
#

GNOME_LIBDIR="-L/usr/local/lib"
GNOME_INCLUDEDIR="-I/usr/local/include -DNEED_GNOMESUPPORT_H -I/usr/local/lib/gnome-libs/include -I/usr/X11R6/include -I/usr/local/lib/glib/include -I/usr/local/include"
GNOME_LIBS="-lgnome -lgnomesupport  -ldb -L/usr/local/lib -rdynamic -lgmodule  -lglib -ldl -ldl"
GNOMEUI_LIBS="-lgnomeui -L/usr/local/lib -lgdk_imlib -L/usr/local/lib -L/usr/X11R6/lib -lgtk -lgdk -lglib -ldl -lgmodule -lXext -lX11 -lm -ljpeg -ltiff -lungif -lpng -lz -lm -lSM -lICE -L/usr/local/lib -L/usr/X11R6/lib -lgtk -lgdk -lglib -ldl -lgmodule -lXext -lX11 -lm -lgnome -lgnomesupport  -ldb -L/usr/local/lib -rdynamic -lgmodule  -lglib -ldl -ldl"
GTKXMHTML_LIBS="-lgtkxmhtml -lXpm -ljpeg -lpng -lz -lz -lSM -lICE -L/usr/local/lib -L/usr/X11R6/lib -lgtk -lgdk -lglib -ldl -lgmodule -lXext -lX11 -lm"

need_gnome_support="yes"
