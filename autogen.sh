#!/bin/sh
# autogen.sh for GtkICQ


srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GtkICQ"

(test -f $srcdir/configure.in && \
 test -f $srcdir/gtkicqrc \
) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

. $srcdir/macros/autogen.sh
