#include <string.h>
#include "gtkicq.h"
#include "gtkfunc.h"

void change_nick( GtkWidget *widget, struct sokandlb *data );

void change_nick( GtkWidget *widget, struct sokandlb *data )
{
	char *new_nick;
	int index;

#ifdef TRACE_FUNCTION
	printf( "change_nick\n" );
#endif
	
	index = GPOINTER_TO_INT(gtk_object_get_data( GTK_OBJECT( widget ),
						     "index" ));

	new_nick = gtk_entry_get_text( GTK_ENTRY( widget ) );
	strcpy( Contacts[ index ].nick, new_nick );
	
	gtk_clist_set_text( GTK_CLIST( data->lb_userwin ),
	                    Contacts[ index ].lb_index, 1, new_nick );
}

void change_nick_window( GtkWidget *widget, struct sokandlb *data )
{
	GtkWidget *window;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *box;
	GtkWidget *button;
	GtkCList *clist = GTK_CLIST( data->lb_userwin );
	int cx;

#ifdef TRACE_FUNCTION
	printf( "change_nick_window\n" );
#endif
	
	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( GPOINTER_TO_INT( clist->selection->data ) == 
		    Contacts[ cx ].lb_index )
		{
			break;
		}
	}

	window = gtk_window_new( GTK_WINDOW_DIALOG );
	gtk_container_border_width( GTK_CONTAINER( window ), 10 );
	gtk_window_set_title( GTK_WINDOW( window ), "GtkICQ: Rename Contact" );

	box = gtk_hbox_new( FALSE, 0 );
	gtk_container_add( GTK_CONTAINER( window ), box );
	gtk_widget_show( box );
	
	label = gtk_label_new( "Name:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1, 0.5 );
	gtk_box_pack_start( GTK_BOX( box ), label, FALSE, FALSE, 5 );
	gtk_widget_show( label );
	
	entry = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY( entry ), Contacts[ cx ].nick );
	gtk_box_pack_start( GTK_BOX( box ), entry, FALSE, FALSE, 5 );
	gtk_signal_connect( GTK_OBJECT( entry ), "changed",
	                    GTK_SIGNAL_FUNC( change_nick ), data );
	gtk_signal_connect_object( GTK_OBJECT( entry ), "activate",
	                           (GtkSignalFunc) gtk_widget_destroy,
	                           GTK_OBJECT( window ) );
	gtk_object_set_data( GTK_OBJECT( entry ), "index",
			     GINT_TO_POINTER( cx ) );
	gtk_widget_show( entry );
	
	button = gtk_button_new_with_label( "OK" );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked", (GtkSignalFunc) gtk_widget_destroy, GTK_OBJECT( window ) );
	gtk_box_pack_start( GTK_BOX( box ), button, FALSE, FALSE, 5 );
	gtk_widget_set_usize( button, 100, 30 );
	gtk_widget_show( button );	

	gtk_widget_show( window );
}
