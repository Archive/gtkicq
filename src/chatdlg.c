#include "gtkicq.h"
#include "tcp.h"
#include "chatdlg.h"

typedef struct
{
	gpointer data;
	int i;
} dataandint;

void SetForeground( GtkWidget *widget, dataandint *data );
void SetBackground( GtkWidget *widget, dataandint *data );
void CreateSetFGWindow( GtkWidget *widget, gpointer data );
void CreateSetBGWindow( GtkWidget *widget, gpointer data );

void SetForeground( GtkWidget *widget, dataandint *data )
{
	static GtkStyle *style;
	GdkColor *foreground, *background;

	GtkColorSelection *colorsel;
	gdouble color[4];
	BYTE rgb[5];
	int cx;

#ifdef TRACE_FUNCTION
	printf( "SetForeground\n" );
#endif

	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( data->i == Contacts[ cx ].chat_sok )
			break;
	}

	colorsel = GTK_COLOR_SELECTION( GTK_COLOR_SELECTION_DIALOG( data->data )->colorsel );
	gtk_color_selection_get_color( colorsel, color );

	rgb[0] = 0x00;
	chat_fg_red = rgb[1] = (BYTE)( 255.0F * color[0] );
	chat_fg_green = rgb[2] = (BYTE)( 255.0F * color[1] );
	chat_fg_blue = rgb[3] = (BYTE)( 255.0F * color[2] );
	rgb[4] = 0x00;

	foreground = (GdkColor *)malloc( sizeof( GdkColor ) );
	background = (GdkColor *)malloc( sizeof( GdkColor ) );

	foreground->red = 256 * chat_fg_red;
	foreground->green = 256 * chat_fg_green;
	foreground->blue = 256 * chat_fg_blue;
	foreground->pixel = (gulong)(
	                    chat_fg_red * 65536 +
	                    chat_fg_green * 256 +
	                    chat_fg_blue );

	background->red = 256 * chat_bg_red;
	background->green = 256 * chat_bg_green;
	background->blue = 256 * chat_bg_blue;
	background->pixel = (gulong)(
	                    chat_bg_red * 65536 +
	                    chat_bg_green * 256 +
	                    chat_bg_blue );

	gdk_color_alloc( gtk_widget_get_colormap( Contacts[ cx ].chat_local_text ), foreground );
	gdk_color_alloc( gtk_widget_get_colormap( Contacts[ cx ].chat_local_text ), background );

	if( !style )
		style = gtk_style_new();
	memcpy( &style->fg[ GTK_STATE_NORMAL ], foreground, sizeof( GdkColor ) );
	memcpy( &style->text[ GTK_STATE_NORMAL ], foreground, sizeof( GdkColor ) );
	memcpy( &style->base[ GTK_STATE_NORMAL ], background, sizeof( GdkColor ) );

	gtk_widget_set_style( Contacts[ cx ].chat_local_text, style );
	
	write( data->i, rgb, 5 );
}

void SetBackground( GtkWidget *widget, dataandint *data )
{
	GtkStyle *style;
	GdkColor *foreground, *background;

	GtkColorSelection *colorsel;
	gdouble color[4];
	BYTE rgb[5];
	int cx;

#ifdef TRACE_FUNCTION
	printf( "SetBackground\n" );
#endif

	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( data->i == Contacts[ cx ].chat_sok )
			break;
	}

	colorsel = GTK_COLOR_SELECTION( GTK_COLOR_SELECTION_DIALOG( data->data )->colorsel );
	gtk_color_selection_get_color( colorsel, color );

	rgb[0] = 0x01;
	chat_bg_red = rgb[1] = (BYTE)( 255.0F * color[0] );
	chat_bg_green = rgb[2] = (BYTE)( 255.0F * color[1] );
	chat_bg_blue = rgb[3] = (BYTE)( 255.0F * color[2] );
	rgb[4] = 0x00;

	foreground = (GdkColor *)malloc( sizeof( GdkColor ) );
	background = (GdkColor *)malloc( sizeof( GdkColor ) );

	foreground->red = 256 * chat_fg_red;
	foreground->green = 256 * chat_fg_green;
	foreground->blue = 256 * chat_fg_blue;
	foreground->pixel = (gulong)(
	                    chat_fg_red * 65536 +
	                    chat_fg_green * 256 +
	                    chat_fg_blue );

	background->red = 256 * chat_bg_red;
	background->green = 256 * chat_bg_green;
	background->blue = 256 * chat_bg_blue;
	background->pixel = (gulong)(
	                    chat_bg_red * 65536 +
	                    chat_bg_green * 256 +
	                    chat_bg_blue );

	gdk_color_alloc( gtk_widget_get_colormap( Contacts[ cx ].chat_local_text ), foreground );
	gdk_color_alloc( gtk_widget_get_colormap( Contacts[ cx ].chat_local_text ), background );

	style = gtk_style_new();
	memcpy( &style->fg[ GTK_STATE_NORMAL ], foreground, sizeof( GdkColor ) );
	memcpy( &style->text[ GTK_STATE_NORMAL ], foreground, sizeof( GdkColor ) );
	memcpy( &style->base[ GTK_STATE_NORMAL ], background, sizeof( GdkColor ) );

	gtk_widget_set_style( Contacts[ cx ].chat_local_text, style );

	write( data->i, rgb, 5 );
}

void CreateSetFGWindow( GtkWidget *widget, gpointer data )
{
	dataandint *pass_data = (dataandint *)malloc( sizeof( dataandint ) );
	GtkWidget *window;
	window = gtk_color_selection_dialog_new( "Set Foreground Color" );

#ifdef TRACE_FUNCTION
	printf( "CreateSetFGWindow\n" );
#endif

	pass_data->i = GPOINTER_TO_INT( data );
	pass_data->data = window;

	gtk_color_selection_set_opacity(
	GTK_COLOR_SELECTION( GTK_COLOR_SELECTION_DIALOG( window )->colorsel ),
	TRUE );
	
	gtk_color_selection_set_update_policy(
	GTK_COLOR_SELECTION( GTK_COLOR_SELECTION_DIALOG( window )->colorsel ),
	GTK_UPDATE_CONTINUOUS );
	
	gtk_signal_connect(
	GTK_OBJECT( GTK_COLOR_SELECTION_DIALOG( window )->ok_button ),
	"clicked", GTK_SIGNAL_FUNC( SetForeground ), pass_data );

	gtk_signal_connect_object(
	GTK_OBJECT( GTK_COLOR_SELECTION_DIALOG( window )->ok_button ),
	"clicked", GTK_SIGNAL_FUNC( gtk_widget_destroy ), GTK_OBJECT( window ) );

	gtk_signal_connect_object(
	GTK_OBJECT( GTK_COLOR_SELECTION_DIALOG( window )->cancel_button ),
	"clicked", GTK_SIGNAL_FUNC( gtk_widget_destroy ), GTK_OBJECT( window ) );

	gtk_widget_show( window );
}

void CreateSetBGWindow( GtkWidget *widget, gpointer data )
{
	dataandint *pass_data = (dataandint *)malloc( sizeof( dataandint ) );
	GtkWidget *window;
	window = gtk_color_selection_dialog_new( "Set Background Color" );

#ifdef TRACE_FUNCTION
	printf( "CreateSetBGWindow\n" );
#endif

	pass_data->i = GPOINTER_TO_INT( data );
	pass_data->data = window;

	gtk_color_selection_set_opacity(
	GTK_COLOR_SELECTION( GTK_COLOR_SELECTION_DIALOG( window )->colorsel ),
	TRUE );
	
	gtk_color_selection_set_update_policy(
	GTK_COLOR_SELECTION( GTK_COLOR_SELECTION_DIALOG( window )->colorsel ),
	GTK_UPDATE_CONTINUOUS );
	
	gtk_signal_connect(
	GTK_OBJECT( GTK_COLOR_SELECTION_DIALOG( window )->ok_button ),
	"clicked", GTK_SIGNAL_FUNC( SetBackground ), pass_data );

	gtk_signal_connect_object(
	GTK_OBJECT( GTK_COLOR_SELECTION_DIALOG( window )->ok_button ),
	"clicked", GTK_SIGNAL_FUNC( gtk_widget_destroy ), GTK_OBJECT( window ) );

	gtk_signal_connect_object(
	GTK_OBJECT( GTK_COLOR_SELECTION_DIALOG( window )->cancel_button ),
	"clicked", GTK_SIGNAL_FUNC( gtk_widget_destroy ), GTK_OBJECT( window ) );

	gtk_widget_show( window );
}

GtkWidget *ChatWindowNew( int cindex, int sock )
{
	GtkWidget *window, *box, *hbox, *textbox, *textbox2, *button;
	GtkWidget *scrollbar;
	GtkWidget *table;
	char wintitle[128];

	time_t ttime;

	GtkStyle *style;

#ifdef TRACE_FUNCTION
	printf( "ChatWindowNew\n" );
#endif

#ifdef CHAT_LOG_FILE
	ttime = time( NULL );

	if( Contacts[ cindex ].chat_file == NULL )
		Contacts[ cindex ].chat_file = fopen( CHAT_LOG_FILE, "a" );
	if( Contacts[ cindex ].chat_file )
		fprintf( Contacts[ cindex ].chat_file, "\n\n%s\n", ctime( &ttime ) );
#endif

	window = gtk_window_new( GTK_WINDOW_DIALOG );
	sprintf( wintitle, "GtkICQ: %s, %s", ( strlen( nickname ) ? nickname : "User" ), Contacts[ cindex ].nick );
	gtk_window_set_title( GTK_WINDOW( window ), wintitle );
	gtk_container_border_width( GTK_CONTAINER( window ), 10 );
	gtk_widget_set_usize( window, 500, 400 );
	gtk_widget_realize( window );

	box = gtk_vbox_new( FALSE, 0 );
	gtk_container_add( GTK_CONTAINER( window ), box );
	gtk_widget_show( box );

	table = gtk_table_new( 2, 2, FALSE );
	gtk_box_pack_start( GTK_BOX( box ), table, TRUE, TRUE, 0 );
	gtk_widget_show( table );
	
	textbox = gtk_text_new( NULL, NULL );
	gtk_text_set_word_wrap( GTK_TEXT( textbox ), TRUE );
	gtk_table_attach( GTK_TABLE( table ), textbox, 0, 1, 0, 1,
	                  GTK_EXPAND | GTK_SHRINK | GTK_FILL,
	                  GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 5 );
	gtk_widget_show( textbox );

	scrollbar = gtk_vscrollbar_new( GTK_TEXT( textbox )->vadj );
	gtk_table_attach( GTK_TABLE( table ), scrollbar, 1, 2, 0, 1,
	                  GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 5 );
	gtk_widget_show( scrollbar );

	textbox2 = gtk_text_new( NULL, NULL );
	gtk_table_attach( GTK_TABLE( table ), textbox2, 0, 1, 1, 2,
	                  GTK_EXPAND | GTK_SHRINK | GTK_FILL,
	                  GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 5 );
	gtk_widget_show( textbox2 );

	scrollbar = gtk_vscrollbar_new( GTK_TEXT( textbox2 )->vadj );
	gtk_table_attach( GTK_TABLE( table ), scrollbar, 1, 2, 1, 2,
	                  GTK_FILL, GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 5 );
	gtk_widget_show( scrollbar );

	gtk_text_set_editable( GTK_TEXT( textbox2 ), TRUE );
	gtk_text_set_word_wrap( GTK_TEXT( textbox2 ), TRUE );
	gtk_signal_connect( GTK_OBJECT( textbox2 ), "key_press_event",
	                    GTK_SIGNAL_FUNC( TCPChatSend ), GINT_TO_POINTER( sock ) );
	gtk_widget_show( textbox2 );

	style = gtk_style_new();
	style->font = gdk_font_load( ChatFontString );
	gtk_widget_set_style( textbox2, style );
	gdk_font_unref( style->font );

	hbox = gtk_hbox_new( FALSE, 0 );

	button = gtk_button_new_with_label( "Set Foreground" );
	gtk_signal_connect( GTK_OBJECT( button ), "clicked",
	                    GTK_SIGNAL_FUNC( CreateSetFGWindow ), GINT_TO_POINTER( sock ) );
	gtk_box_pack_start( GTK_BOX( hbox ), button, FALSE, FALSE, 15 );
	gtk_widget_show( button );

	button = gtk_button_new_with_label( "Set Background" );
	gtk_box_pack_start( GTK_BOX( hbox ), button, FALSE, FALSE, 15 );
	gtk_signal_connect( GTK_OBJECT( button ), "clicked",
	                    GTK_SIGNAL_FUNC( CreateSetBGWindow ), GINT_TO_POINTER( sock ) );
	gtk_widget_show( button );

	button = gtk_button_new_with_label( "Close Chat" );
	gtk_signal_connect( GTK_OBJECT( button ), "clicked",
	                    GTK_SIGNAL_FUNC( TCPTerminateChat ),
	                    GINT_TO_POINTER( sock ) );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked",
	                           GTK_SIGNAL_FUNC( gtk_widget_destroy ),
	                           GTK_OBJECT( window ) );
	gtk_box_pack_start( GTK_BOX( hbox ), button, FALSE, FALSE, 15 );
	gtk_widget_show( button );

	gtk_box_pack_end( GTK_BOX( box ), hbox, FALSE, FALSE, 0 );
	gtk_widget_show( hbox );

	gtk_widget_show( window );

	Contacts[ cindex ].chat_local_text = textbox2;
	Contacts[ cindex ].chat_remote_text = textbox;

	return textbox;
}
