#include "gtkicq.h"
#include <gtk/gtk.h>
#include <time.h>

void log_window_add( char *statement, int stamped, int uin )
{
	char *strings[4];
	char who[32];
	time_t timedate;
	struct tm *my_tm;
	char ptime[16];
	char pdate[16];
	int cx;
	char *halves[] = { "AM", "PM" };
	int half = 0;

#ifdef TRACE_FUNCTION
	printf( "log_window_add\n" );
#endif

	strings[ 0 ] = ptime;
	strings[ 1 ] = pdate;
	strings[ 2 ] = who;
	strings[ 3 ] = statement;

	time( &timedate );
	my_tm = localtime( &timedate );

	if( my_tm->tm_hour > 12 )
	{
		my_tm->tm_hour -= 12 ;
		half ++;
	}

	sprintf( ptime, "%02d:%02d:%02d %s", my_tm->tm_hour, my_tm->tm_min,
	         my_tm->tm_sec, halves[ half ] );
	sprintf( pdate, "%02d/%02d/%d", my_tm->tm_mon + 1, my_tm->tm_mday,
	         my_tm->tm_year );

	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( uin == Contacts[ cx ].uin )
		{
			sprintf( who, "%s", Contacts[ cx ].nick );
			break;
		}
	}
	
	if( cx == Num_Contacts )
	{
		sprintf( who, "%d", uin );
	}

	gtk_clist_insert( GTK_CLIST( log_list ), 0, strings );
}
