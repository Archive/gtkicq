#include "gtkicq.h"
#include "gtkfunc.h"
#include <gnome.h>
#include <gdk_imlib.h>
#include <applet-widget.h>

extern GtkWidget *darea;

void make_applet( int argc, char *argv[], struct sokandlb *data );
void applet_update( DWORD status, GdkPixmap *flash, struct sokandlb *data );
void applet_clicked_cb( GtkWidget *widget, GdkEventButton *ev, gpointer data );
void applet_hide_main( GtkWidget *widget, gpointer data );
