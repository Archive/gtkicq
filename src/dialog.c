#include <string.h>
#include "gtkicq.h"
#include "dialog.h"

void OK_Box( char *header, char *message )
{
	GtkWidget *dialog;
	GtkWidget *text = NULL;
	GtkWidget *button;
	GtkWidget *label;
	GtkWidget *scrollbar;
	GtkWidget *hbox;
	GtkWidget *vbox;

	GtkStyle *style;
	static GdkColor *gray;

#ifdef TRACE_FUNCTION
	printf( "OK_Box\n" );
#endif
	
	gray  = (GdkColor *)malloc(sizeof(GdkColor));
	gray->red = 45000;
	gray->green = 45000;
	gray->blue = 45000;
	gray->pixel = (gulong)255*750000;

	dialog = gtk_window_new( GTK_WINDOW_DIALOG );
	
	gtk_signal_connect( GTK_OBJECT( dialog ), "destroy", GTK_SIGNAL_FUNC( gtk_widget_destroyed ), &dialog );
	
	gtk_window_set_title( GTK_WINDOW( dialog ), "GtkICQ: Message" );
	gtk_container_border_width( GTK_CONTAINER( dialog ), 10 );

	if( strcmp( message, "" ) )
	{
		gtk_widget_set_usize( dialog, 350, 250 );
	}
	
	vbox = gtk_vbox_new( FALSE, 0 );
	gtk_container_add( GTK_CONTAINER( dialog ), vbox );

	label = gtk_label_new( header );
	gtk_box_pack_start( GTK_BOX( vbox ), label, TRUE, TRUE, 5 );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.5, 1 );
	gtk_widget_show( label );	

	if( strcmp( message, "" ) )
	{
		hbox = gtk_hbox_new( FALSE, 0 );
		
		text = gtk_text_new( NULL, NULL );

		gdk_color_alloc( gtk_widget_get_colormap( text ), gray );

		style = gtk_style_new();
		memcpy( &style->base[ GTK_STATE_NORMAL ], gray, sizeof( GdkColor ) );
		gtk_widget_set_style( text, style );

		gtk_text_set_word_wrap( GTK_TEXT( text ), TRUE );

		gtk_box_pack_start( GTK_BOX( hbox ), text, TRUE, TRUE, 0 );
		gtk_widget_show( text );
	
		scrollbar = gtk_vscrollbar_new( GTK_TEXT( text )->vadj );
		gtk_box_pack_start( GTK_BOX( hbox ), scrollbar, FALSE, FALSE, 0 );
		gtk_widget_show( scrollbar );

		gtk_box_pack_start( GTK_BOX( vbox ), hbox, TRUE, TRUE, 5 );
		gtk_widget_show( hbox );
	}

	hbox = gtk_hbox_new( FALSE, 0 );

	button = gtk_button_new_with_label( "OK" );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked", GTK_SIGNAL_FUNC( gtk_widget_destroy ), GTK_OBJECT( dialog ) );
	gtk_widget_set_usize( button, 100, 30 );
	gtk_box_pack_end( GTK_BOX( hbox ), button, TRUE, FALSE, 5 );
	gtk_widget_show( button );
	
	gtk_box_pack_end( GTK_BOX( vbox ), hbox, FALSE, FALSE, 5 );
	gtk_widget_show( hbox );
	
	gtk_widget_show( vbox );

	gtk_widget_show( dialog );

	if( strcmp( message, "" ) )
	{
		gtk_widget_realize( text );
		gtk_text_freeze( GTK_TEXT( text ) );
		gtk_text_insert( GTK_TEXT( text ), 0, 0, 0, message, -1 );		
		gtk_text_thaw( GTK_TEXT( text ) );
	}
}
