#include "gtkicq.h"

GtkWidget *main_menu( struct sokandlb *data )
{
	GtkWidget *menubar, *menubar_options, *menubar_status;
	GtkWidget *menu, *menuitem_search, *menuitem_history, *menuitem_options,
	          *menuitem, *menuitem_quit;
	GtkWidget *menuitem_online, *menuitem_away, *menuitem_na, *menuitem_ffc,
	          *menuitem_occ, *menuitem_dnd, *menuitem_inv, *menuitem_offline;

#ifdef TRACE_FUNCTION
	printf( "main_menu\n" );
#endif

	menubar = gtk_menu_bar_new();
	menubar_options = gtk_menu_item_new_with_label( "ICQ" );

	menu = gtk_menu_new();
	menuitem_search = gtk_menu_item_new_with_label( "Add Contact" );
	menuitem_history = gtk_menu_item_new_with_label( "Connection History" );
	menuitem_options = gtk_menu_item_new_with_label( "Options" );
	menuitem = gtk_menu_item_new();
	menuitem_quit = gtk_menu_item_new_with_label( "Quit" );
	gtk_menu_item_set_submenu( GTK_MENU_ITEM( menubar_options ), menu );
	gtk_menu_bar_append( GTK_MENU_BAR( menubar ), menubar_options );
	gtk_menu_append( GTK_MENU( menu ), menuitem_search );
	gtk_menu_append( GTK_MENU( menu ), menuitem_history );
	gtk_menu_append( GTK_MENU( menu ), menuitem_options );
	gtk_menu_append( GTK_MENU( menu ), menuitem );
	gtk_menu_append( GTK_MENU( menu ), menuitem_quit );
	gtk_widget_show( menuitem_search );
	gtk_widget_show( menuitem_history );
	gtk_widget_show( menuitem );
	gtk_widget_show( menuitem_options );
	gtk_widget_show( menuitem_quit );
	gtk_widget_show( menubar_options );

	menu = gtk_menu_new();
	menubar_status = gtk_menu_item_new_with_label( "Status" );

	menuitem_online = gtk_menu_item_new_with_label( "Online" );
	menuitem_away = gtk_menu_item_new_with_label( "Away" );
	menuitem_na = gtk_menu_item_new_with_label( "Not Available" );
	menuitem_ffc = gtk_menu_item_new_with_label( "Free for Chat" );
	menuitem_occ = gtk_menu_item_new_with_label( "Occupied" );
	menuitem_dnd = gtk_menu_item_new_with_label( "Do Not Disturb" );
	menuitem_inv = gtk_menu_item_new_with_label( "Invisible" );
	menuitem = gtk_menu_item_new();
	menuitem_offline = gtk_menu_item_new_with_label( "Offline" );

	gtk_menu_item_set_submenu( GTK_MENU_ITEM( menubar_status ), menu );
	gtk_menu_bar_append( GTK_MENU_BAR( menubar ), menubar_status );
	gtk_menu_append( GTK_MENU( menu ), menuitem_online );
	gtk_menu_append( GTK_MENU( menu ), menuitem_away );
	gtk_menu_append( GTK_MENU( menu ), menuitem_na );
	gtk_menu_append( GTK_MENU( menu ), menuitem_ffc );
	gtk_menu_append( GTK_MENU( menu ), menuitem_occ );
	gtk_menu_append( GTK_MENU( menu ), menuitem_dnd );
	gtk_menu_append( GTK_MENU( menu ), menuitem_inv );
	gtk_menu_append( GTK_MENU( menu ), menuitem );
	gtk_menu_append( GTK_MENU( menu ), menuitem_offline );

	gtk_widget_show( menuitem_online );
	gtk_widget_show( menuitem_away );
	gtk_widget_show( menuitem_na );
	gtk_widget_show( menuitem_ffc );
	gtk_widget_show( menuitem_occ );
	gtk_widget_show( menuitem_dnd );
	gtk_widget_show( menuitem_inv );
	gtk_widget_show( menuitem );
	gtk_widget_show( menuitem_offline );
	gtk_widget_show( menubar_status );
	
	gtk_signal_connect_object( GTK_OBJECT( menuitem_search ), "activate",
	                           GTK_SIGNAL_FUNC( search_window ), (gpointer)data );
	
	gtk_signal_connect_object( GTK_OBJECT( menuitem_quit ), "activate",
	                           GTK_SIGNAL_FUNC( icq_quit_object ), (gpointer)data );

	gtk_signal_connect( GTK_OBJECT( menuitem_online ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_online ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_offline ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_offline ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_away ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_away ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_na ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_na ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_inv ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_invisible ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_dnd ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_dnd ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_occ ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_occ ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_ffc ), "activate",
	                    GTK_SIGNAL_FUNC( icq_set_status_ffc ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_options ), "activate",
	                    GTK_SIGNAL_FUNC( configure_window ), data );
	gtk_signal_connect( GTK_OBJECT( menuitem_history ), "activate",
	                    GTK_SIGNAL_FUNC( toggle_log_window ), NULL );
	
	return menubar;
}
