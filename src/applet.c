#include <config.h>
#include <string.h>
#include <gdk_imlib.h>
#include <applet-widget.h>
#include "applet.h"
#include "pixmaps.h"
#include "loadpixmap.h"

#ifdef GNOME
#include <gnome.h>

GtkWidget *frame;
GtkWidget *frame_outline;
GtkWidget *mess_label;
GtkWidget *mess_pm;

static PanelOrientType orient;

void applet_exposed_cb( GtkWidget *widget, gpointer data );
GtkWidget *create_applet( void );
void init_applet_signals( GtkWidget *window, struct sokandlb *data );
static void applet_change_orient( GtkWidget *w, PanelOrientType o, gpointer data );

void applet_exposed_cb( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	printf( "applet_exposed_cb\n" );
#endif
	applet_update( Current_Status, NULL, data );
}

void applet_update( DWORD status, GdkPixmap *flash, struct sokandlb *data )
{
	static GtkTooltips *tt = NULL;
	GdkPixmap *status_pix;
	GdkBitmap *status_bit;
	char *status_string;
	char *message_count;
	int num_messages = 0;
	int cx;
	char *tooltips_str;

#ifdef TRACE_FUNCTION
	printf( "applet_update\n" );
#endif

	if( applet_toggle == FALSE )
		return;

	if( tt == NULL )
		tt = gtk_tooltips_new();

	message_count = (char *)malloc( 16 );
	tooltips_str = (char *)malloc( 64 );

	if( status == STATUS_OFFLINE )
	{
		status_pix = icon_offline_pixmap;
		status_bit = icon_offline_bitmap;
	}
	else switch( status & 0xffff )
	{
		case STATUS_ONLINE:
			status_pix = icon_online_pixmap;
			status_bit = icon_online_bitmap;
			break;
		case STATUS_AWAY:
			status_pix = icon_away_pixmap;
			status_bit = icon_away_bitmap;
			break;
		case STATUS_NA:
			status_pix = icon_na_pixmap;
			status_bit = icon_na_bitmap;
			break;
		case STATUS_DND:
			status_pix = icon_dnd_pixmap;
			status_bit = icon_dnd_bitmap;
			break;
		case STATUS_FREE_CHAT:
			status_pix = icon_ffc_pixmap;
			status_bit = icon_ffc_bitmap;
			break;
		case STATUS_OCCUPIED:
			status_pix = icon_occ_pixmap;
			status_bit = icon_occ_bitmap;
			break;
		case STATUS_INVISIBLE:
			status_pix = icon_inv_pixmap;
			status_bit = icon_inv_bitmap;
			break;
		default:
			status_pix = icon_offline_pixmap;
			status_bit = icon_offline_bitmap;
			break;
	}

	for( cx = 0; cx < Num_Contacts; cx ++ )
		num_messages += Contacts[ cx ].messages;

	if( flash != NULL )
		gtk_pixmap_set( GTK_PIXMAP( mess_pm ), icon_message_pixmap,
		                icon_message_bitmap );
	else if( num_messages )
		gtk_pixmap_set( GTK_PIXMAP( mess_pm ), icon_blank_pixmap,
		                icon_blank_bitmap );
	else
		gtk_pixmap_set( GTK_PIXMAP( mess_pm ), status_pix,
		                status_bit );


	sprintf( message_count, "%d", num_messages );

	gtk_label_set( GTK_LABEL( mess_label ), message_count );

	sprintf( tooltips_str, "%ld: %d User(s) Online", UIN, data->online );
	gtk_tooltips_set_tip( tt, frame, tooltips_str, NULL );
	gtk_tooltips_enable( tt );

	free( message_count );
	free( tooltips_str );
}

void applet_hide_main( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	printf( "applet_hide_main\n" );
#endif

	if( !GTK_WIDGET_VISIBLE( app ) )
		gtk_widget_show( app );
	else
		gtk_widget_hide( app );
}


void applet_clicked_cb( GtkWidget *widget, GdkEventButton *ev, gpointer data )
{
#ifdef TRACE_FUNCTION
	printf( "applet_clicked_cb\n" );
#endif

	if( ev == NULL )
		return;

	if( ev->button != 1 || ev->type != GDK_2BUTTON_PRESS )
		return;

	if( !GTK_WIDGET_VISIBLE( app ) )
		gtk_widget_show( app );
	else
		gtk_widget_hide( app );
}

void init_applet_signals( GtkWidget *window, struct sokandlb *data )
{
	gtk_widget_set_events( window, gtk_widget_get_events( window ) |
	                       GDK_BUTTON_PRESS_MASK | GDK_EXPOSURE_MASK );
	gtk_signal_connect( GTK_OBJECT( window ), "expose_event",
	                    GTK_SIGNAL_FUNC( applet_exposed_cb ), data );
	gtk_signal_connect( GTK_OBJECT( window ), "button_press_event",
	                    GTK_SIGNAL_FUNC( applet_clicked_cb ), NULL );
	gtk_signal_connect( GTK_OBJECT( window ), "change_orient",
	                    GTK_SIGNAL_FUNC( applet_change_orient ), NULL );
}

GtkWidget *create_applet( void )
{
#if 0
	GtkStyle *style;
#endif
	GtkWidget *hbox;
	GtkWidget *table;
	GtkWidget *eventbox;

#ifdef TRACE_FUNCTION
	printf( "create_applet\n" );
#endif

	if( mess_label == NULL )
		mess_label = gtk_label_new( "0" );

	if( mess_pm == NULL )
		mess_pm = gtk_pixmap_new( icon_blank_pixmap, icon_blank_bitmap );

	gtk_widget_push_visual (gdk_imlib_get_visual ());
	gtk_widget_push_colormap (gdk_imlib_get_colormap ());

	hbox = gtk_hbox_new( FALSE, 0 );
	table = gtk_table_new( 2, 1, FALSE );
	eventbox = gtk_event_box_new();
	gtk_container_add( GTK_CONTAINER( eventbox ), hbox );
	gtk_widget_show( hbox );

	frame_outline = gtk_frame_new( NULL );
	
	frame = gtk_fixed_new();
	gtk_widget_set_usize( frame, 22, 44 );

	gtk_container_add( GTK_CONTAINER( frame_outline ), frame );
	gtk_frame_set_shadow_type(GTK_FRAME( frame_outline ), GTK_SHADOW_IN );
	gtk_widget_set_usize( frame_outline, 24, 48 );
	gtk_widget_show( frame_outline );

	gtk_box_pack_start( GTK_BOX( hbox ), frame, FALSE, FALSE, 0 );

/*	gtk_container_add( GTK_CONTAINER( frame ), table );*/
	gtk_widget_show( table );
	gtk_widget_show( frame );

/*	gtk_table_attach_defaults( GTK_TABLE( table ), mess_pm, 0, 1, 0, 1 );*/
	gtk_fixed_put( GTK_FIXED( frame ), mess_pm, 0, 0 );
	gtk_widget_show( mess_pm );

/*	gtk_table_attach_defaults( GTK_TABLE( table ), mess_label, 0, 1, 1, 2 );*/
	gtk_misc_set_alignment( GTK_MISC( mess_label ), 0.5, 0.5 );
	gtk_fixed_put( GTK_FIXED( frame ), mess_label, 5, 22 );
	gtk_widget_show( mess_label );

	gtk_widget_pop_colormap ();
	gtk_widget_pop_visual ();
        return eventbox;
}

static char *cfnp;

void
make_applet( int argc, char *argv[], struct sokandlb *data )
{
	int cx;

	GtkStyle *style;

	static struct poptOption arguments[] =
	{
		{"config", 'c', POPT_ARG_STRING, &cfnp, 0, N_("Use this file instead of ~/.icq/gtkicqrc"), N_("CONFIG")},
		{"noapplet", 'a', POPT_ARG_NONE, 0, 0, N_("startup without applet support"), NULL},
		{NULL, 0, 0, NULL, 0, NULL, NULL}
	};

	GtkWidget *widget;

	GtkWidget *applet;

#ifdef TRACE_FUNCTION
	printf( "make_applet\n" );
#endif

	for( cx = 0; cx < argc; cx ++ )
	{
		if( !strcmp( "-a", argv[ cx ] ) ||
		    !strcmp( "--noapplet", argv[ cx ] ) )
			applet_toggle = FALSE;
	}

	if( applet_toggle == FALSE )
		return;

	cfnp = 0;
	applet_widget_init( "gtkicq", VERSION, argc, argv,
	                    arguments, 0, NULL );

	if( cfnp )
		strncpy( configfilename, cfnp, 255 );

	applet = applet_widget_new( "gtkicq_applet" );
	if ( !applet )
		g_error( _( "Can't create applet!\n" ) );

	style = gtk_widget_get_style( applet );

	init_applet_signals( applet, data );
	gtk_widget_realize( applet );
	init_pixmaps( style, applet );
	widget = create_applet();
	gtk_widget_show( widget );

	applet_widget_add( APPLET_WIDGET( applet ), widget );

	gtk_widget_show( applet );
}

/* A lot of this code was ripped out of the modemlights applet, so I can't
 * take credit for it myself :)
 */
static void applet_change_orient( GtkWidget *w, PanelOrientType o, gpointer data )
{
	static int first_call_flag = FALSE;
	
	orient = o;
	
	if( !first_call_flag )
	{
		first_call_flag = TRUE;
		return;
	}

	if( orient == ORIENT_LEFT || orient == ORIENT_RIGHT )
	{
		gtk_widget_set_usize( frame, 44, 22 );
		gtk_fixed_move( GTK_FIXED( frame ), mess_label, 30, 0 );
	}
	else
	{
		gtk_widget_set_usize( frame, 22, 44 );
		gtk_fixed_move( GTK_FIXED( frame ), mess_label, 6, 22 );
	}
}

#endif
