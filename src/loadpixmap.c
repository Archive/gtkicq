#include "gtkicq.h"
#include "pixmaps.h"
#include "loadpixmap.h"

#include "xpms/gtkicq-blank.xpm"
#include "xpms/gtkicq-message.xpm"
#include "xpms/gtkicq-url.xpm"
#include "xpms/gtkicq-auth.xpm"
#include "xpms/gtkicq-away.xpm"
#include "xpms/gtkicq-na.xpm"
#include "xpms/gtkicq-occ.xpm"
#include "xpms/gtkicq-dnd.xpm"
#include "xpms/gtkicq-ffc.xpm"
#include "xpms/gtkicq-inv.xpm"
#include "xpms/gtkicq-online.xpm"
#include "xpms/gtkicq-offline.xpm"
#include "xpms/gtkicq-chat.xpm"
#include "xpms/gtkicq-chat2.xpm"
#include "xpms/gtkicq-nomess.xpm"

void init_pixmaps( GtkStyle *style, GtkWidget *main_window )
{
#ifdef TRACE_FUNCTION
	printf( "init_pixmaps\n" );
#endif

	icon_blank_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                  &icon_blank_bitmap,
	                                                  &style->bg[ GTK_STATE_NORMAL ],
	                                                  (gchar **)icon_blank_xpm );

	icon_message_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                    &icon_message_bitmap,
	                                                    &style->bg[ GTK_STATE_NORMAL ],
	                                                    (gchar **)icon_message_xpm );

	icon_url_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                &icon_url_bitmap,
	                                                &style->bg[ GTK_STATE_NORMAL ],
	                                                (gchar **)icon_url_xpm );

	icon_auth_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                 &icon_auth_bitmap,
	                                                 &style->bg[ GTK_STATE_NORMAL ],
	                                                 (gchar **)icon_auth_xpm );

	icon_away_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                 &icon_away_bitmap,
	                                                 &style->bg[ GTK_STATE_NORMAL ],
	                                                 (gchar **)icon_away_xpm );

	icon_na_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                               &icon_na_bitmap,
	                                               &style->bg[ GTK_STATE_NORMAL ],
	                                               (gchar **)icon_na_xpm );

	icon_occ_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                &icon_occ_bitmap,
	                                                &style->bg[ GTK_STATE_NORMAL ],
	                                                (gchar **)icon_occ_xpm );

	icon_dnd_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                &icon_dnd_bitmap,
	                                                &style->bg[ GTK_STATE_NORMAL ],
	                                                (gchar **)icon_dnd_xpm );

	icon_ffc_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                &icon_ffc_bitmap,
	                                                &style->bg[ GTK_STATE_NORMAL ],
	                                                (gchar **)icon_ffc_xpm );

	icon_inv_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                &icon_inv_bitmap,
	                                                &style->bg[ GTK_STATE_NORMAL ],
	                                                (gchar **)icon_inv_xpm );

	icon_online_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                   &icon_online_bitmap,
	                                                   &style->bg[ GTK_STATE_NORMAL ],
	                                                   (gchar **)icon_online_xpm );
	icon_offline_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                    &icon_offline_bitmap,
	                                                    &style->bg[ GTK_STATE_NORMAL ],
	                                                    (gchar **)icon_offline_xpm );
	icon_chat_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                 &icon_chat_bitmap,
	                                                 &style->bg[ GTK_STATE_NORMAL],
	                                                 (gchar **)icon_chat_xpm );

	icon_chat2_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                                  &icon_chat2_bitmap,
	                                                  &style->bg[ GTK_STATE_NORMAL],
	                                                  (gchar **)icon_chat2_xpm );

	nomess_pixmap = gdk_pixmap_create_from_xpm_d( main_window->window,
	                                              &nomess_bitmap,
	                                              &style->bg[ GTK_STATE_NORMAL],
	                                              (gchar **)nomess_xpm );
}
