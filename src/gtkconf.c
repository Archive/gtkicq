/* GtkICQ configuration functions Originally by Paul Laufer */
/* Minorly modified by Jeremy Wise */

#include <stdio.h>
#include <stdlib.h>
#include "gtkicq.h"

#include <gtk/gtk.h>
#include <gdk/gdk.h>

#ifdef GNOME
void color_set_online(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_offline(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_away(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_na(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_ffc(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_occ(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_dnd(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
void color_set_inv(GnomeColorPicker *cp, guint pr, guint pg, guint pb );
#endif

void toggle_switch(GtkWidget *widget, int *toggle_me);
void save_changes(GtkWidget *widget, struct sokandlb *data );
void file_browse(GtkWidget *widget, GtkWidget *entry);
void got_name(GtkWidget *widget, GtkWidget *entry);
void widget_destroy(GtkWidget *widget, GtkWidget *gonner);

/* Temporary values so changes are NOT saved unless "Save" is pressed */

int temp_packet_toggle = FALSE;
int temp_force_toggle = FALSE;
int temp_sound_toggle = FALSE;
int temp_UserOnline = FALSE;
int temp_UserOffline = FALSE;
int temp_RecvMessage = FALSE;
int temp_RecvChat = FALSE;
GdkColor temp_color_online;
GdkColor temp_color_offline;
GdkColor temp_color_away;
GdkColor temp_color_na;
GdkColor temp_color_ffc;
GdkColor temp_color_occ;
GdkColor temp_color_dnd;
GdkColor temp_color_inv;


GdkColor color_online, color_offline, color_away, color_na, color_ffc, color_occ, color_dnd, color_inv;
int color_online_set = 0, color_offline_set = 0, color_away_set = 0, color_na_set = 0, color_ffc_set = 0, color_occ_set = 0, color_dnd_set = 0, color_inv_set = 0;

GtkWidget *onlinefile, *offlinefile, *recvfile, *chatfile, *icqserver, *portnumber, *uin, *nick, *pass, *filesel;
GtkWidget *onlinebutton, *offlinebutton, *recvbutton, *chatbutton;
GtkWidget *onlinebrowse, *offlinebrowse, *recvbrowse, *chatbrowse;

#ifdef GNOME
void color_set_online(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_online.red = pr;
	temp_color_online.green = pg;
	temp_color_online.blue = pb;
}

void color_set_offline(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_offline.red = pr;
	temp_color_offline.green = pg;
	temp_color_offline.blue = pb;
}

void color_set_away(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_away.red = pr;
	temp_color_away.green = pg;
	temp_color_away.blue = pb;
}

void color_set_na(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_na.red = pr;
	temp_color_na.green = pg;
	temp_color_na.blue = pb;
}

void color_set_ffc(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_ffc.red = pr;
	temp_color_ffc.green = pg;
	temp_color_ffc.blue = pb;
}

void color_set_occ(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_occ.red = pr;
	temp_color_occ.green = pg;
	temp_color_occ.blue = pb;
}

void color_set_dnd(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_dnd.red = pr;
	temp_color_dnd.green = pg;
	temp_color_dnd.blue = pb;
}

void color_set_inv(GnomeColorPicker *cp, guint pr, guint pg, guint pb ) 
{
	temp_color_inv.red = pr;
	temp_color_inv.green = pg;
	temp_color_inv.blue = pb;
}
#endif

void init_colors( struct sokandlb *data )
{
#ifdef TRACE_FUNCTION
	printf( "init_colors\n" );
#endif

	/* Online is blue */
	if( color_online_set == FALSE )
	{
		color_online.red = 0;
		color_online.green = 0;
		color_online.blue = 30000;
		color_online.pixel = (gulong)(255);
	}
	
	/* Offline is dark gray */
	if( color_offline_set == FALSE )
	{
		color_offline.red = 30000;
		color_offline.green = 30000;
		color_offline.blue = 30000;
		color_offline.pixel = (gulong)(255*50000);
	}

	/* Away is green */
	if( color_away_set == FALSE )
	{
		color_away.red = 0;
		color_away.green = 30000;
		color_away.blue = 0;
		color_away.pixel = (gulong)255*256;
	}
	
	/* Not available is green also */
	if( color_na_set == FALSE )
	{
		color_na.red = 0;
		color_na.green = 30000;
		color_na.blue = 0;
		color_na.pixel = (gulong)255*256;
	}

	/* Free for chat is blue */
	if( color_ffc_set == FALSE )
	{
		color_ffc.red = 0;
		color_ffc.green = 0;
		color_ffc.blue = 30000;
		color_ffc.pixel = (gulong)(255);
	}

	/* Occupied is green */
	if( color_occ_set == FALSE )
	{
		color_occ.red = 0;
		color_occ.green = 30000;
		color_occ.blue = 0;
		color_occ.pixel = (gulong)255*256;
	}

	/* Do not disturb is green also */
	if( color_dnd_set == FALSE )
	{
		color_dnd.red = 0;
		color_dnd.green = 30000;
		color_dnd.blue = 0;
		color_dnd.pixel = (gulong)255*256;
	}

	/* Invisible is blue */
	if( color_inv_set == FALSE )
	{
		color_inv.red = 0;
		color_inv.green = 0;
		color_inv.blue = 30000;
		color_inv.pixel = (gulong)(255);
	}

	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_online );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_offline );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_away );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_na );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_ffc );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_occ );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_dnd );
	gdk_color_alloc( gtk_widget_get_colormap( data->lb_userwin ), &color_inv );
}

void configure_window(GtkWidget *widget, struct sokandlb *data )
{
	GtkWidget *window;
	GtkWidget *button;
	GtkWidget *vbox, *hbox, *vboxmain;
	GtkWidget *notebook, *label;

#ifdef TRACE_FUNCTION
	printf( "configure_window\n" );
#endif

	temp_packet_toggle = packet_toggle;
	temp_force_toggle = force_toggle;
	temp_sound_toggle = sound_toggle;
	temp_UserOnline = UserOnline;
	temp_UserOffline = UserOffline;
	temp_RecvMessage = RecvMessage;
	temp_RecvChat = RecvChat;

	memcpy( &temp_color_online, &color_online, sizeof( GdkColor ) );
	memcpy( &temp_color_offline, &color_offline, sizeof( GdkColor ) );
	memcpy( &temp_color_away, &color_away, sizeof( GdkColor ) );
	memcpy( &temp_color_na, &color_na, sizeof( GdkColor ) );
	memcpy( &temp_color_ffc, &color_ffc, sizeof( GdkColor ) );
	memcpy( &temp_color_occ, &color_occ, sizeof( GdkColor ) );
	memcpy( &temp_color_dnd, &color_dnd, sizeof( GdkColor ) );
	memcpy( &temp_color_inv, &color_inv, sizeof( GdkColor ) );

	/* our configure window */
	window = gtk_window_new(GTK_WINDOW_DIALOG);
	gtk_window_set_title(GTK_WINDOW(window), "GtkICQ: Options");
	gtk_widget_set_usize( window, 430, 230 );
	gtk_signal_connect(GTK_OBJECT(window), "destroy", GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);
	gtk_container_border_width(GTK_CONTAINER(window), 5);
	
	/* vbox main - holds notebook and the buttons at the end */
	vboxmain = gtk_vbox_new(FALSE, 5);
	gtk_container_add(GTK_CONTAINER(window), vboxmain);

	/* notebook contains pages of setup info - very nice :) */
	notebook = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
	gtk_box_pack_start(GTK_BOX(vboxmain), notebook, FALSE, FALSE, 0);

	/* hbox holds columns . . . */
	hbox = gtk_hbox_new(FALSE, 0);
	
	/* 1 vbox column of info . . . */
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);
	
/* start sound notebook page */
	/* add page to notebook */
	label = gtk_label_new("Sound");
	gtk_notebook_prepend_page(GTK_NOTEBOOK(notebook), hbox, label);

	/* sound master */
	button = gtk_check_button_new_with_label("Enable Sound");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), temp_sound_toggle);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_sound_toggle);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
	gtk_widget_show(button);
	
	/* online sound */
	onlinebutton = gtk_check_button_new_with_label("User Online:");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(onlinebutton), temp_UserOnline);
	gtk_signal_connect(GTK_OBJECT(onlinebutton), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_UserOnline);
	gtk_box_pack_start(GTK_BOX(vbox), onlinebutton, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( onlinebutton, temp_sound_toggle );
	gtk_widget_show(onlinebutton);

	/* offline sound */
	offlinebutton = gtk_check_button_new_with_label("User Offline:");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(offlinebutton), temp_UserOffline);
	gtk_signal_connect(GTK_OBJECT(offlinebutton), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_UserOffline);
	gtk_box_pack_start(GTK_BOX(vbox), offlinebutton, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( offlinebutton, temp_sound_toggle );
	gtk_widget_show(offlinebutton);

	/* Recieve message sound button */
	recvbutton = gtk_check_button_new_with_label("Rec'd Message:");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(recvbutton), temp_RecvMessage);
	gtk_signal_connect(GTK_OBJECT(recvbutton), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_RecvMessage);
	gtk_box_pack_start(GTK_BOX(vbox), recvbutton, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( recvbutton, temp_sound_toggle );
	gtk_widget_show(recvbutton);

	/* Recieve chat sound button */
	chatbutton = gtk_check_button_new_with_label("Rec'd Chat Request:");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(chatbutton), temp_RecvChat);
	gtk_box_pack_start(GTK_BOX(vbox), chatbutton, FALSE, FALSE, 5);
	gtk_signal_connect(GTK_OBJECT(chatbutton), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_RecvChat);
	gtk_widget_set_sensitive( chatbutton, temp_sound_toggle );
	gtk_widget_show(chatbutton);

	/* start next column */
	gtk_widget_show(vbox);
	vbox = gtk_vbox_new(FALSE, 2);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

	/* add some space so that the fields line up - a better way to do this? */
	button = gtk_label_new("");
	gtk_widget_set_usize(button, -1, 23);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
	
	/* Online sound file */
	onlinefile = gtk_entry_new_with_max_length( 255 );
	gtk_widget_set_usize( onlinefile, 200, -1 );
	gtk_entry_set_text(GTK_ENTRY(onlinefile), UserOnlineSound);
	gtk_box_pack_start(GTK_BOX(vbox), onlinefile, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( onlinefile, ( temp_UserOnline && temp_sound_toggle ) );
	gtk_widget_show(onlinefile);
	
	/* Offline sound file */
	offlinefile = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(offlinefile, 200, -1);
	gtk_entry_set_text(GTK_ENTRY(offlinefile), UserOfflineSound);
	gtk_box_pack_start(GTK_BOX(vbox), offlinefile, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( offlinefile, ( temp_UserOffline && temp_sound_toggle ) );
	gtk_widget_show(offlinefile);
	
	/* Message sound file */
	recvfile = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(recvfile, 200, -1);
	gtk_entry_set_text(GTK_ENTRY(recvfile), RecvMessageSound );
	gtk_box_pack_start(GTK_BOX(vbox), recvfile, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( recvfile, ( temp_RecvMessage && temp_sound_toggle ) );
	gtk_widget_show(recvfile);

	/* Chat sound file */
	chatfile = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(chatfile, 200, -1);
	gtk_entry_set_text(GTK_ENTRY(chatfile), RecvChatSound);
	gtk_box_pack_start(GTK_BOX(vbox), chatfile, FALSE, FALSE, 5);
	gtk_widget_set_sensitive( chatfile, ( temp_RecvChat && temp_sound_toggle ) );
	gtk_widget_show(chatfile);
	
	gtk_widget_show(vbox);
	/* now let us test some file dialogue boxes :) */
	vbox = gtk_vbox_new(FALSE, 2);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 15 );

	/* add some space */
	button = gtk_label_new("");
	gtk_widget_set_usize(button, -1, 25);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
	
	/* add browse button for OnlineSound name */
	onlinebrowse = gtk_button_new_with_label("Browse");
	gtk_signal_connect(GTK_OBJECT(onlinebrowse), "clicked", GTK_SIGNAL_FUNC(file_browse), onlinefile);
	gtk_widget_set_sensitive( onlinebrowse, ( temp_UserOnline && temp_sound_toggle ) );
	gtk_widget_show(onlinebrowse);
	gtk_box_pack_start(GTK_BOX(vbox), onlinebrowse, FALSE, FALSE, 5);

	/* add browse button for OfflineSound name */
	offlinebrowse = gtk_button_new_with_label("Browse");
	gtk_signal_connect(GTK_OBJECT(offlinebrowse), "clicked", GTK_SIGNAL_FUNC(file_browse), offlinefile);
	gtk_widget_set_sensitive( offlinebrowse, ( temp_UserOffline && temp_sound_toggle ) );
	gtk_widget_show(offlinebrowse);
	gtk_box_pack_start(GTK_BOX(vbox), offlinebrowse, FALSE, FALSE, 5);

	/* add browse button for MessageSound name */
	recvbrowse = gtk_button_new_with_label("Browse");
	gtk_signal_connect(GTK_OBJECT(recvbrowse), "clicked", GTK_SIGNAL_FUNC(file_browse), recvfile);
	gtk_widget_set_sensitive( recvbrowse, ( temp_RecvMessage && temp_sound_toggle ) );
	gtk_widget_show(recvbrowse);
	gtk_box_pack_start(GTK_BOX(vbox), recvbrowse, FALSE, FALSE, 5);

	/* add browse button for ChatSound name */
	chatbrowse = gtk_button_new_with_label("Browse");
	gtk_signal_connect(GTK_OBJECT(chatbrowse), "clicked", GTK_SIGNAL_FUNC(file_browse), chatfile);
	gtk_widget_set_sensitive( chatbrowse, ( temp_RecvChat && temp_sound_toggle ) );
	gtk_widget_show(chatbrowse);
	gtk_box_pack_start(GTK_BOX(vbox), chatbrowse, FALSE, FALSE, 5);

	gtk_widget_show(vbox);	
	gtk_widget_show(hbox);
	
/* Start network notebook page */
	hbox = gtk_hbox_new(FALSE, 5); /* so that the entry fields are right justified */
	vbox = gtk_vbox_new(FALSE, 4);  /* to help line up the labels and entry fields */
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);
	label = gtk_label_new("Network");
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), hbox, label);

	/* dump button */
	button = gtk_check_button_new_with_label("Dump Packets");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), temp_packet_toggle);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_packet_toggle);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0 );
	gtk_widget_show(button);

	/* force button */
	button = gtk_check_button_new_with_label("Force through Server");
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(button), temp_force_toggle);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(toggle_switch), &temp_force_toggle);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0 );
	gtk_widget_show(button);

	/* Label for icq server */
	button = gtk_label_new("ICQ Server:");
	gtk_misc_set_alignment( GTK_MISC( button ), 1.0, 0.5 );
	gtk_widget_set_usize(button, -1, 18);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
	gtk_widget_show(button);

	/* Label for icq port */
	button = gtk_label_new("Port Number:");
	gtk_misc_set_alignment( GTK_MISC( button ), 1.0, 0.5 );
	gtk_widget_set_usize(button, -1, 18);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
	gtk_widget_show(button);

	/* end column 1 */
	gtk_widget_show(vbox);
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

	/* insert 1 line */
	button = gtk_label_new("");
	gtk_widget_set_usize(button, -1, 25);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0 );

	/* And line 2 */
	button = gtk_label_new("");
	gtk_widget_set_usize(button, -1, 25);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0 );

	/* icq server */
	icqserver = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(icqserver, 200, -1);
	gtk_entry_set_text(GTK_ENTRY(icqserver), server);
	gtk_box_pack_start(GTK_BOX(vbox), icqserver, FALSE, FALSE, 5);
	gtk_widget_show(icqserver);

	/* icq server port */
	portnumber = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(portnumber, 200, -1);
	{
		char temp[256];
		sprintf(temp, "%ld", remote_port);
		gtk_entry_set_text(GTK_ENTRY(portnumber), temp);
	}
	gtk_box_pack_start(GTK_BOX(vbox), portnumber, FALSE, FALSE, 5);
	gtk_widget_show(portnumber);
		
	gtk_widget_show(vbox);
	gtk_widget_show(hbox);
	/* end network page */
	
/* now let's do the User Info page :) */
	hbox = gtk_hbox_new(FALSE, 5); /* so that the entry fields are right justified */
	vbox = gtk_vbox_new(FALSE, 5);  /* to help lign up the labels and entry fields */
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);
	label = gtk_label_new("User Info");
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), hbox, label);

	/* add UIN label */
	button = gtk_label_new("UIN:");
	gtk_misc_set_alignment( GTK_MISC( button ), 1.0, 0.5 );
	gtk_widget_set_usize(button, -1, 18);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
	gtk_widget_show(button);
	
	/* add Nickname label */
	button = gtk_label_new("Nickname:");
	gtk_misc_set_alignment( GTK_MISC( button ), 1.0, 0.5 );
	gtk_widget_set_usize(button, -1, 18);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
	gtk_widget_show(button);

	/* add password label */
	button = gtk_label_new("Password:");
	gtk_misc_set_alignment( GTK_MISC( button ), 1.0, 0.5 );
	gtk_widget_set_usize(button, -1, 18);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 5);
	gtk_widget_show(button);
	
	gtk_widget_show(vbox);
	/* end 1st column */
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 5);

	/* UIN entry box */
	uin = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(uin, 200, -1);
	{
		char temp[256];
		sprintf(temp, "%ld", UIN);
		gtk_entry_set_text(GTK_ENTRY(uin), temp);
	}
	gtk_box_pack_start(GTK_BOX(vbox), uin, FALSE, FALSE, 5);
	gtk_widget_show(uin);

	/* Nickname box */
	nick = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(nick, 200, -1);
	gtk_entry_set_text(GTK_ENTRY(nick), nickname);
	gtk_box_pack_start(GTK_BOX(vbox), nick, FALSE, FALSE, 5);
	gtk_widget_show(nick);
	
	/* Password box */
	pass = gtk_entry_new_with_max_length(255);
	gtk_widget_set_usize(pass, 200, -1);
	gtk_entry_set_text(GTK_ENTRY(pass), passwd);
	gtk_box_pack_start(GTK_BOX(vbox), pass, FALSE, FALSE, 5);
	gtk_widget_show(pass);
	gtk_entry_set_visibility(GTK_ENTRY(pass), FALSE); /* don't show world password :) */

	gtk_widget_show(vbox);
	gtk_widget_show(hbox);
	/* done with User info */

#ifdef GNOME
/* Now we'll add the color box, currently supported only for Gnome */
	hbox = gtk_hbox_new(FALSE, 26); /* so that the entry fields are right justified */
	vbox = gtk_table_new( 4, 4, FALSE );
	gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

	label = gtk_label_new("Color");
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), hbox, label);

	label = gtk_label_new( "Online:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 0, 1, 0, 1,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "Offline:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 0, 1, 1, 2,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );
	
	label = gtk_label_new( "Away:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 0, 1, 2, 3,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );
	
	label = gtk_label_new( "Not Available:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 0, 1, 3, 4,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );
	
	gtk_widget_show( vbox );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_online.red,
	           temp_color_online.green,
	           temp_color_online.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_online), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 1, 2, 0, 1, 0, 0, 5, 5 );
	gtk_widget_show( button );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_offline.red,
	           temp_color_offline.green,
	           temp_color_offline.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT( button ), "color_set",
	                   GTK_SIGNAL_FUNC( color_set_offline ), NULL );
	gtk_table_attach( GTK_TABLE( vbox ), button, 1, 2, 1, 2, 0, 0, 5, 5 );
	gtk_widget_show( button );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_away.red,
	           temp_color_away.green,
	           temp_color_away.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_away), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 1, 2, 2, 3, 0, 0, 5, 5 );
	gtk_widget_show( button );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_na.red,
	           temp_color_na.green,
	           temp_color_na.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_na), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 1, 2, 3, 4, 0, 0, 5, 5 );
	gtk_widget_show( button );

	label = gtk_label_new( "Free for Chat:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 2, 3, 0, 1,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "Occupied:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 2, 3, 1, 2,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );
	
	label = gtk_label_new( "Do not Disturb:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 2, 3, 2, 3,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );
	
	label = gtk_label_new( "Invisible:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( vbox ), label, 2, 3, 3, 4,
	                  GTK_FILL | GTK_EXPAND, 0, 0, 0 );
	gtk_widget_show( label );
	
	gtk_widget_show( vbox );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_ffc.red,
	           temp_color_ffc.green,
	           temp_color_ffc.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_ffc), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 3, 4, 0, 1, 0, 0, 5, 5 );
	gtk_widget_show( button );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_occ.red,
	           temp_color_occ.green,
	           temp_color_occ.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_occ), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 3, 4, 1, 2, 0, 0, 5, 5 );
	gtk_widget_show( button );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_dnd.red,
	           temp_color_dnd.green,
	           temp_color_dnd.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_dnd), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 3, 4, 2, 3, 0, 0, 5, 5 );
	gtk_widget_show( button );

	button = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER( button ), 
	           temp_color_inv.red,
	           temp_color_inv.green,
	           temp_color_inv.blue,
	           0 );
	gtk_signal_connect(GTK_OBJECT(button), "color_set",
	                   GTK_SIGNAL_FUNC(color_set_inv), NULL);
	gtk_table_attach( GTK_TABLE( vbox ), button, 3, 4, 3, 4, 0, 0, 5, 5 );
	gtk_widget_show( button );

	gtk_widget_show( hbox );
#endif

/* done with notebook stuff, now do the buttons */
	
	hbox = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vboxmain), hbox, FALSE ,FALSE, 0);
	
	/* save button */
	button = gtk_button_new_with_label("Save");
	gtk_widget_set_usize(button, 70, -1);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
	                   GTK_SIGNAL_FUNC(save_changes), data );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked",
	                           GTK_SIGNAL_FUNC( gtk_widget_destroy ),
	                           GTK_OBJECT( window ) );
	gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
	gtk_widget_show(button);
	
	/* cancel button */
	button = gtk_button_new_with_label("Cancel");
	gtk_widget_set_usize(button, 70, -1);
	gtk_signal_connect_object(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(window));
	gtk_box_pack_start(GTK_BOX(hbox), button, FALSE, FALSE, 0);
	gtk_widget_show(button);
	
	gtk_widget_show(notebook);
	gtk_widget_show(hbox);
	gtk_widget_show(vboxmain);
	gtk_widget_show(window);
}

void toggle_switch(GtkWidget *widget, int *toggle_me)
{
#ifdef TRACE_FUNCTION
	printf( "toggle_switch\n" );
#endif

	*toggle_me = !*toggle_me;
	if( toggle_me == &temp_sound_toggle && *toggle_me == FALSE )
	{
		gtk_widget_set_sensitive( onlinebutton, FALSE );
		gtk_widget_set_sensitive( offlinebutton, FALSE );
		gtk_widget_set_sensitive( recvbutton, FALSE );
		gtk_widget_set_sensitive( chatbutton, FALSE );
		gtk_widget_set_sensitive( onlinefile, FALSE );
		gtk_widget_set_sensitive( offlinefile, FALSE );
		gtk_widget_set_sensitive( recvfile, FALSE );
		gtk_widget_set_sensitive( chatfile, FALSE );
		gtk_widget_set_sensitive( onlinebrowse, FALSE );
		gtk_widget_set_sensitive( offlinebrowse, FALSE );
		gtk_widget_set_sensitive( recvbrowse, FALSE );
		gtk_widget_set_sensitive( chatbrowse, FALSE );
	}
	else if( toggle_me == &temp_sound_toggle )
	{
		gtk_widget_set_sensitive( onlinebutton, TRUE );
		gtk_widget_set_sensitive( offlinebutton, TRUE );
		gtk_widget_set_sensitive( recvbutton, TRUE );
		gtk_widget_set_sensitive( chatbutton, TRUE );
		gtk_widget_set_sensitive( onlinefile, temp_UserOnline );
		gtk_widget_set_sensitive( offlinefile, temp_UserOffline );
		gtk_widget_set_sensitive( recvfile, temp_RecvMessage );
		gtk_widget_set_sensitive( chatfile, temp_RecvChat );
	}
	else if( toggle_me == &temp_UserOnline )
	{
		gtk_widget_set_sensitive( onlinefile, *toggle_me );
		gtk_widget_set_sensitive( onlinebrowse, *toggle_me );
	}
	else if( toggle_me == &temp_UserOffline )
	{
		gtk_widget_set_sensitive( offlinefile, *toggle_me );
		gtk_widget_set_sensitive( offlinebrowse, *toggle_me );
	}
	else if( toggle_me == &temp_RecvMessage )
	{
		gtk_widget_set_sensitive( recvfile, *toggle_me );
		gtk_widget_set_sensitive( recvbrowse, *toggle_me );
	}
	else if( toggle_me == &temp_RecvChat )
	{
		gtk_widget_set_sensitive( chatfile, *toggle_me );
		gtk_widget_set_sensitive( chatbrowse, *toggle_me );
	}
}

void save_changes(GtkWidget *widget, struct sokandlb *data )
{
#ifdef TRACE_FUNCTION
	printf( "save_changes\n" );
#endif

	/* these will be written to file when the user exits gtkicq
	 * since the rcfile is rewritten every time. Don't ask me why... */
	strcpy(UserOnlineSound, gtk_entry_get_text(GTK_ENTRY(onlinefile)));
	strcpy(UserOfflineSound, gtk_entry_get_text(GTK_ENTRY(offlinefile)));
	strcpy(RecvMessageSound, gtk_entry_get_text(GTK_ENTRY(recvfile)));
	strcpy(RecvChatSound, gtk_entry_get_text(GTK_ENTRY(chatfile)));
	
	strcpy(server, gtk_entry_get_text(GTK_ENTRY(icqserver)));
	remote_port = atoi(gtk_entry_get_text(GTK_ENTRY(portnumber)));

	UIN = atoi(gtk_entry_get_text(GTK_ENTRY(uin)));
	strcpy(nickname, gtk_entry_get_text(GTK_ENTRY(nick)));
	strcpy(passwd, gtk_entry_get_text(GTK_ENTRY(pass)));

	packet_toggle = temp_packet_toggle;
	force_toggle = temp_force_toggle;
	sound_toggle = temp_sound_toggle;
	UserOnline = temp_UserOnline;
	UserOffline = temp_UserOffline;
	RecvChat = temp_RecvChat;
	RecvMessage = temp_RecvMessage;

	memcpy( &color_online, &temp_color_online, sizeof( GdkColor ) );
	memcpy( &color_offline, &temp_color_offline, sizeof( GdkColor ) );
	memcpy( &color_away, &temp_color_away, sizeof( GdkColor ) );
	memcpy( &color_na, &temp_color_na, sizeof( GdkColor ) );
	memcpy( &color_ffc, &temp_color_ffc, sizeof( GdkColor ) );
	memcpy( &color_occ, &temp_color_occ, sizeof( GdkColor ) );
	memcpy( &color_dnd, &temp_color_dnd, sizeof( GdkColor ) );
	memcpy( &color_inv, &temp_color_inv, sizeof( GdkColor ) );

	Build_Main_Window( data );
/*	Save_RC();*/
}

void file_browse(GtkWidget *widget, GtkWidget *entry)
{
#ifdef TRACE_FUNCTION
	printf( "file_browse\n" );
#endif

	filesel = gtk_file_selection_new("GtkICQ: Select Sound");
	gtk_file_selection_set_filename(GTK_FILE_SELECTION(filesel), gtk_entry_get_text(GTK_ENTRY(onlinefile)));
	gtk_signal_connect(GTK_OBJECT(filesel), "destroy", GTK_SIGNAL_FUNC(gtk_widget_destroy), NULL);
	gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(filesel)->ok_button), "clicked", GTK_SIGNAL_FUNC(got_name), entry);
	gtk_signal_connect(GTK_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button), "clicked", GTK_SIGNAL_FUNC(widget_destroy), filesel);
	gtk_widget_show(filesel);
}

void got_name(GtkWidget *widget, GtkWidget *entry)
{
#ifdef TRACE_FUNCTION
	printf( "got_name\n" );
#endif

	gtk_entry_set_text( GTK_ENTRY( entry ),
	                    gtk_file_selection_get_filename( GTK_FILE_SELECTION( filesel ) ) );
	gtk_widget_destroy( filesel );
}

/* needed this because otherwise pressing the cancel button in file selection just removed the cancel button */
void widget_destroy(GtkWidget *widget, GtkWidget *gonner)
{
#ifdef TRACE_FUNCTION
	printf( "widget_destroy\n" );
#endif

	gtk_widget_destroy(gonner);
}
