#include <string.h>
#include "gtkicq.h"

GtkWidget *contents;
GtkWidget *mainbox;
GtkWidget *next_button;
GtkWidget *nick_entry = NULL,
          *uin_entry = NULL,
          *password_entry = NULL,
          *first_entry = NULL,
          *last_entry = NULL,
          *email_entry = NULL,
          *auth_entry = NULL;
int nextid;

void NewUserFinish( GtkWidget *widget, gpointer data );
void NewUserCancel( GtkWidget *widget, gpointer data );
void NewUser2( GtkWidget *widget, GtkRadioButton *radiobutton );

void NewUserFinish( GtkWidget *widget, gpointer data )
{
	static int been_called = 0;

#ifdef TRACE_FUNCTION
	printf( "NewUserFinish\n" );
#endif
	
	if( been_called )
		gtk_main_quit();
	else
		been_called = 1;
}

void NewUserCancel( GtkWidget *widget, gpointer data )
{
#ifdef TRACE_FUNCTION
	printf( "NewUserCancel\n" );
#endif

	gtk_main_quit();
	exit( 1 );
}

void NewUser2( GtkWidget *widget, GtkRadioButton *radiobutton )
{
	GtkWidget *label = NULL;

	int selected;

#ifdef TRACE_FUNCTION
	printf( "NewUser2\n" );
#endif

	selected = (int)GTK_TOGGLE_BUTTON( radiobutton )->active;

	gtk_widget_destroy( contents );
	gtk_signal_disconnect( GTK_OBJECT( next_button ), nextid );

	gtk_signal_connect( GTK_OBJECT( next_button ), "clicked",
	                    GTK_SIGNAL_FUNC( NewUserFinish ), NULL );
	contents = gtk_table_new( 2, 7, FALSE );
	gtk_box_pack_start( GTK_BOX( mainbox ), contents, FALSE, FALSE, 40 );

	label = gtk_label_new( "               UIN: " );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 0, 1, GTK_FILL, 0, 0, 10 );
	gtk_widget_show( label );

	label = gtk_label_new( "           Password: " );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 1, 2, GTK_FILL, 0, 0, 10 );
	gtk_widget_show( label );

	label = gtk_label_new( "           Nickname: " );
	gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
	gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 2, 3, GTK_FILL, 0, 0, 10 );
	gtk_widget_show( label );

	if( !selected )
	{
		label = gtk_label_new( "           First Name: " );
		gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
		gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 3, 4, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( label );

		label = gtk_label_new( "           Last Name: " );
		gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
		gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 4, 5, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( label );

		label = gtk_label_new( "           Email Address: " );
		gtk_misc_set_alignment( GTK_MISC( label ), 1.0, 0.5 );
		gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 5, 6, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( label );
	}

	uin_entry = gtk_entry_new();
	gtk_table_attach( GTK_TABLE( contents ), uin_entry, 1, 2, 0, 1, GTK_FILL, 0, 0, 10 );

	if( !selected )
		gtk_widget_set_sensitive( uin_entry, FALSE );
	gtk_widget_show( uin_entry );

	password_entry = gtk_entry_new_with_max_length( 8 );
	gtk_table_attach( GTK_TABLE( contents ), password_entry, 1, 2, 1, 2, GTK_FILL, 0, 0, 10 );
	gtk_widget_show( password_entry );

	nick_entry = gtk_entry_new();
	gtk_table_attach( GTK_TABLE( contents ), nick_entry, 1, 2, 2, 3, GTK_FILL, 0, 0, 10 );
	gtk_widget_show( nick_entry );

	if( !selected )
	{
		first_entry = gtk_entry_new();
		gtk_table_attach( GTK_TABLE( contents ), first_entry, 1, 2, 3, 4, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( first_entry );

		last_entry = gtk_entry_new();
		gtk_table_attach( GTK_TABLE( contents ), last_entry, 1, 2, 4, 5, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( last_entry );

		email_entry = gtk_entry_new();
		gtk_table_attach( GTK_TABLE( contents ), email_entry, 1, 2, 5, 6, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( email_entry );

		auth_entry = gtk_check_button_new_with_label( "Require Authorization" );
		gtk_table_attach( GTK_TABLE( contents ), auth_entry, 1, 2, 6, 7, GTK_FILL, 0, 0, 10 );
		gtk_widget_show( auth_entry );
	}

	gtk_widget_show( contents );

	if( !selected )
	{
		is_new_user = TRUE;
	}
}

void NewUserSignup( void )
{
	GtkWidget *window;
	GtkWidget *label;
	GtkWidget *radiobutton;
	GtkWidget *box, *box2;
	GtkWidget *button;

#ifdef TRACE_FUNCTION
	printf( "NewUserSignup\n" );
#endif

	window = gtk_window_new( GTK_WINDOW_DIALOG );
	gtk_window_set_title( GTK_WINDOW( window ), "GtkICQ: New User" );

	mainbox = gtk_vbox_new( FALSE, 0 );
	gtk_container_add( GTK_CONTAINER( window ), mainbox );
	gtk_widget_show( mainbox );
	
	contents = gtk_table_new( 3, 1, FALSE );
	gtk_box_pack_start( GTK_BOX( mainbox ), contents, FALSE, FALSE, 0 );
	
	label = gtk_label_new( "Welcome to GtkICQ" );
	gtk_table_attach( GTK_TABLE( contents ), label, 0, 1, 0, 1,
	                  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 5 );
	gtk_widget_show( label );
	
	box = gtk_vbox_new( FALSE, 0 );

	box2 = gtk_vbox_new( FALSE, 0 );
	gtk_table_attach( GTK_TABLE( contents ), box2, 0, 1, 1, 2,
	                  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 15, 5 );
	gtk_widget_show( box2 );

	label = gtk_label_new( "Welcome to GtkICQ.  To assist you with" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "configuring this program, please fill" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "out the following information.  You will" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "not need to do this again, unless you" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "somehow remove your ~/.icq/gtkicqrc file." );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	box2 = gtk_vbox_new( FALSE, 0 );
	radiobutton = gtk_radio_button_new( NULL );

	label = gtk_label_new( "New ICQ # - Select this if you've never used" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "ICQ before, or if you don't remember your" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	label = gtk_label_new( "nick or password." );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	gtk_container_add( GTK_CONTAINER( radiobutton ), box2 );
	gtk_widget_show( box2 );

	gtk_table_attach( GTK_TABLE( contents ), box, 0, 1, 2, 3,
	                  GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 5 );
	gtk_box_pack_start( GTK_BOX( box ), radiobutton, FALSE, FALSE, 10 );
	gtk_widget_show( radiobutton );
	
	box2 = gtk_vbox_new( FALSE, 0 );
	radiobutton = gtk_radio_button_new( gtk_radio_button_group( GTK_RADIO_BUTTON( radiobutton ) ) );

	label = gtk_label_new( "Existing ICQ # - Select this if you've already" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );
	
	label = gtk_label_new( "got an account which you'd like to use with" );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );
	
	label = gtk_label_new( "GtkICQ." );
	gtk_misc_set_alignment( GTK_MISC( label ), 0.0, 0.0 );
	gtk_box_pack_start( GTK_BOX( box2 ), label, FALSE, FALSE, 0 );
	gtk_widget_show( label );

	gtk_container_add( GTK_CONTAINER( radiobutton ), box2 );
	gtk_widget_show( box2 );

	gtk_box_pack_start( GTK_BOX( box ), radiobutton, FALSE, FALSE, 10 );
	gtk_widget_show( radiobutton );

	box2 = gtk_hbutton_box_new();
	gtk_button_box_set_layout( GTK_BUTTON_BOX( box2 ),
	                           GTK_BUTTONBOX_SPREAD );
	gtk_box_pack_end( GTK_BOX( mainbox ), box2, FALSE, FALSE, 10 );
	gtk_widget_show( box2 );

	next_button = gtk_button_new_with_label( "Next" );
	gtk_widget_set_usize( next_button, 100, 30 );
	gtk_container_add( GTK_CONTAINER( box2 ), next_button );

	nextid = gtk_signal_connect( GTK_OBJECT( next_button ), "clicked",
	                             GTK_SIGNAL_FUNC( NewUser2 ), radiobutton );
	GTK_WIDGET_SET_FLAGS( next_button, GTK_CAN_DEFAULT );
	gtk_widget_grab_default( next_button );
	gtk_widget_show( next_button );
	
	button = gtk_button_new_with_label( "Cancel" );
	gtk_widget_set_usize( button, 100, 30 );
	gtk_container_add( GTK_CONTAINER( box2 ), button );

	gtk_signal_connect( GTK_OBJECT( button ), "clicked",
	                    GTK_SIGNAL_FUNC( NewUserCancel ), NULL );
	gtk_widget_show( button );

	gtk_widget_show( box );
	gtk_widget_show( contents );
	gtk_widget_show( window );

	gtk_main();

	UIN = atoi( gtk_entry_get_text( GTK_ENTRY( uin_entry ) ) );
	sprintf( nickname, "%s", gtk_entry_get_text( GTK_ENTRY( nick_entry ) ) );
	sprintf( passwd, "%s", gtk_entry_get_text( GTK_ENTRY( password_entry ) ) );

	if( first_entry != NULL )
	{
		our_user_info.nick = (char*)malloc( strlen( gtk_entry_get_text( GTK_ENTRY( nick_entry ) ) ) );
		strcpy( our_user_info.nick, gtk_entry_get_text( GTK_ENTRY( nick_entry ) ) );
		our_user_info.first = (char*)malloc( strlen( gtk_entry_get_text( GTK_ENTRY( first_entry ) ) ) );
		strcpy( our_user_info.first, gtk_entry_get_text( GTK_ENTRY( first_entry ) ) );
		our_user_info.last = (char*)malloc( strlen( gtk_entry_get_text( GTK_ENTRY( last_entry ) ) ) );
		strcpy( our_user_info.last, gtk_entry_get_text( GTK_ENTRY( last_entry ) ) );
		our_user_info.email = (char*)malloc( strlen( gtk_entry_get_text( GTK_ENTRY( email_entry ) ) ) );
		strcpy( our_user_info.email, gtk_entry_get_text( GTK_ENTRY( email_entry ) ) );
		our_user_info.auth = (int)GTK_TOGGLE_BUTTON( auth_entry )->active;
	}
	gtk_widget_destroy( window );
}
