#include "gtkicq.h"

#ifndef _TCP_H
#define _TCP_H

#define ICQ_CMDxTCP_START             0x07EE
#define ICQ_CMDxTCP_CANCEL            0x07D0
#define ICQ_CMDxTCP_ACK               0x07DA
#define ICQ_CMDxTCP_MSG               0x0001
#define ICQ_CMDxTCP_FILE              0x0003
#define ICQ_CMDxTCP_CHAT              0x0002
#define ICQ_CMDxTCP_URL               0x0004
#define ICQ_CMDxTCP_READxAWAYxMSG     0x03E8
#define ICQ_CMDxTCP_READxOCCxMSG      0x03E9
#define ICQ_CMDxTCP_READxNAxMSG       0x03EA
#define ICQ_CMDxTCP_READxDNDxMSG      0x03EB
#define ICQ_CMDxTCP_HANDSHAKE         0x03FF
#define ICQ_CMDxTCP_HANDSHAKE2        0x04FF
#define ICQ_CMDxTCP_HANDSHAKE3        0x02FF

#define ICQ_ACKxTCP_ONLINE            0x0000
#define ICQ_ACKxTCP_AWAY              0x0004
#define ICQ_ACKxTCP_NA                0x000E
#define ICQ_ACKxTCP_DND               0x000A
#define ICQ_ACKxTCP_OCC               0x0009
#define ICQ_ACKxTCP_REFUSE            0x0001

#define FONT_PLAIN                    0x00000000
#define FONT_BOLD                     0x00000001
#define FONT_ITALICS                  0x00000002
#define FONT_UNDERLINE                0x00000004

extern const unsigned long int LOCALHOST;

int TCPReadPacket( struct sokandlb *data, int sock, GdkInputCondition cond );
int TCPSendChatRequest( DWORD uin, char *msg, struct sokandlb *data );
int TCPSendMessage( DWORD uin, char *msg, struct sokandlb *data );
int TCPSendURL( DWORD uin, char *msg, struct sokandlb *data );
int TCPAcceptIncoming( struct sokandlb *data, int sock, GdkInputCondition cond );
int TCPAcceptChat( int sock, int cindex, DWORD seq );
int TCPRefuseChat( int sock, int cindex, DWORD seq );
int TCPChatSend( GtkWidget *widget, GdkEventKey *ev, int sock );
void TCPTerminateChat( GtkWidget *widget, gpointer data );
int TCPRetrieveAwayMessage( int cindex, struct sokandlb *data );

#endif
