#ifndef _GTKFUNC_H
#define _GTKFUNC_H

struct sokandlb
{
	int sok;
	GtkWidget *lb_userwin;
#if 0
	GtkWidget *lb_syswin;
#endif
	GtkWidget *window;
	int online;
	int offline;
};

struct URLInfo
{
	struct sokandlb *data;
	GtkWidget *url;
	GtkWidget *desc;
	int cindex;
};

extern GtkWidget *found_list;
void icq_quit_object( struct sokandlb *data );
void icq_quit( GtkWidget *widget, struct sokandlb *data );
void icq_refresh( struct sokandlb *data,
                  int sok, GdkInputCondition cond );
void icq_msgbox( char *message_text, char *sender,
                 struct sokandlb *data );
GtkWidget *create_menu ( struct sokandlb *data );
void icq_sendmessage_window( GtkWidget *clist, GdkEventButton *event,
                             struct sokandlb *data );
int stay_connected( struct sokandlb *data );
void icq_sendmessage( GtkWidget *button, struct sokandlb *data );
void show_personal_info( GtkWidget *widget, struct sokandlb *data );
void rename_uin( GtkWidget *widget, struct sokandlb *data );
void search_window( GtkWidget *widget, struct sokandlb *data );
void display_mess_history( GtkWidget *widget, struct sokandlb *data );
void update_personal_history( char *message, int index );
void change_nick_window( GtkWidget *widget, struct sokandlb *data );
void change_away_window( GtkWidget *widget, gpointer data );
void read_next( GtkWidget *widget, struct sokandlb *data );
#if 0
void icq_sysmessage_window( GtkWidget *clist, GdkEventButton *event,
                            struct sokandlb *data );
#endif
void request_chat( GtkWidget *widget, struct sokandlb *data );
void send_url_window( GtkWidget *widget, struct sokandlb *data );
void remove_user( GtkWidget *widget, struct sokandlb *data );
void show_info_new( SOK_T sok, int uin );
void retrieve_away_message( GtkWidget *widget, struct sokandlb *data );

void icq_set_status_online( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_offline( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_away( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_na( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_invisible( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_dnd( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_occ( GtkWidget *widget, struct sokandlb *data );
void icq_set_status_ffc( GtkWidget *widget, struct sokandlb *data );
void add_to_inv_list( GtkWidget *widget, struct sokandlb *data );
void remove_from_inv_list( GtkWidget *widget, struct sokandlb *data );
void add_to_vis_list( GtkWidget *widget, struct sokandlb *data );
void remove_from_vis_list( GtkWidget *widget, struct sokandlb *data );
void change_winsize( GtkWidget *widget, gpointer data );
void icq_sendmessage_window_from_menu( GtkWidget *widget,
                                       struct sokandlb *data );

#endif
