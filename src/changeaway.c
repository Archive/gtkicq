#ifdef HAVE_CONFIG_H
	#include <config.h>
#endif

#include <string.h>
#include "gtkicq.h"
#include "gtkfunc.h"

void change_away( GtkWidget *widget, struct sokandlb *data );

void change_away( GtkWidget *widget, struct sokandlb *data )
{
	char *new_away;

#ifdef TRACE_FUNCTION
	printf( "change_away\n" );
#endif

	new_away = gtk_entry_get_text( GTK_ENTRY( widget ) );
	strcpy( Away_Message, new_away );
}

void change_away_window( GtkWidget *widget, gpointer data )
{
	GtkWidget *window;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *box;
	GtkWidget *button;

#ifdef TRACE_FUNCTION
	printf( "change_away_window\n" );
#endif
	
	window = gtk_window_new( GTK_WINDOW_DIALOG );
	gtk_container_border_width( GTK_CONTAINER( window ), 10 );
	gtk_window_set_title( GTK_WINDOW( window ), "GtkICQ: Set Away Message" );

	box = gtk_hbox_new( FALSE, 0 );
	gtk_container_add( GTK_CONTAINER( window ), box );
	gtk_widget_show( box );
	
	label = gtk_label_new( "Away Message:" );
	gtk_misc_set_alignment( GTK_MISC( label ), 1, 0.5 );
	gtk_box_pack_start( GTK_BOX( box ), label, FALSE, FALSE, 5 );
	gtk_widget_show( label );
	
	entry = gtk_entry_new();
	gtk_entry_set_text( GTK_ENTRY( entry ), Away_Message );
	gtk_box_pack_start( GTK_BOX( box ), entry, FALSE, FALSE, 5 );
	gtk_signal_connect( GTK_OBJECT( entry ), "changed",
	                    GTK_SIGNAL_FUNC( change_away ), NULL );
	gtk_signal_connect_object( GTK_OBJECT( entry ), "activate",
	                           (GtkSignalFunc) gtk_widget_destroy,
	                           GTK_OBJECT( window ) );
	gtk_widget_show( entry );
	
	button = gtk_button_new_with_label( "OK" );
	gtk_signal_connect_object( GTK_OBJECT( button ), "clicked",
	                           (GtkSignalFunc) gtk_widget_destroy,
	                           GTK_OBJECT( window ) );
	gtk_box_pack_start( GTK_BOX( box ), button, FALSE, FALSE, 5 );
	gtk_widget_set_usize( button, 100, 30 );
	gtk_widget_show( button );	

	gtk_widget_show( window );
}
