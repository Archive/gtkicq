/*
 *	Config File Parser
 *
 *	Jordan Nelson (jordan@pathwaynet.com) 08/08/1998
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gtkicq.h"

void str2col(GdkColor *color, char *str) {
    guint r, g, b;
    gulong pix;

    sscanf(str, "#%04x%04x%04x%04x", &r, &g, &b, &pix);
    color->red = r;
    color->green = g;
    color->blue = b;
    color->pixel = pix;
}
 
int Read_RC_File( void )
{
	FILE *fp;
	char buffer[ 1536 ];
	char label[ 512 ];
	char value[ 512 ];
	char section[ 512 ];
	char single[ 2 ];
	char rcfile[ 256 ];

#ifdef TRACE_FUNCTION
	printf( "Read_RC_File\n" );
#endif

	strcpy( rcfile, getenv( "HOME" ) );
	strcat( rcfile, "/.icq/gtkicqrc" );

	if( strlen( configfilename ) )
		strcpy( rcfile, configfilename );
	
	if( ( fp = fopen( rcfile, "r" ) ) == NULL )
	{
		printf( "Error opening %s\n", rcfile );
		exit( 1 );
	}
	
	Contact_List = FALSE;
	
	while( !feof( fp ) )
	{
		fgets( buffer, 1024, fp );
		
		if( buffer[ 0 ] != '#' || buffer[ 0 ] != '\n' )
		{
			sscanf( buffer, "%s %[^\n]", label, value );
			strip_quote( value );
			
			if( !strcmp( label, "Section" ) )
			{
				strcpy( section, value );
			}
			else if( !strcmp( label, "EndSection" ) )
			{
				strcpy( section, "" );
			}

			if( !strcmp( section, "Personal" ) )
			{
				if( !strcmp( label, "UIN" ) )
				{
					UIN = atoi( value );
				}
				else if( !strcmp( label, "Password" ) )
				{
					strcpy( passwd, value );
				}
				else if( !strcmp( label, "Status" ) )
				{
					Current_Status = atoi( value );
				}
				else if( !strcmp( label, "Nickname" ) )
				{
					strncpy( nickname, value, 29 );
				}
			}
			else if( !strcmp( section, "Server" ) ) 
			{
				if( !strcmp( label, "Server" ) )
				{
					strcpy( server, value );
				}
				else if( !strcmp( label, "Port" ) )
				{
					remote_port = atoi( value );
				}
			}
			else if( !strcmp( section, "Status" ) )
			{
				if( !strcmp( label, "AWAY" ) )
				{
					strcpy( Away_Message, value );
				}
				else if( !strcmp( label, "WindowSize" ) )
				{
					sscanf( value, "%dx%d", &WindowWidth, &WindowHeight );
				}
				else if( !strcmp( label, "WindowTitle" ) )
				{
					strcpy( WindowTitle, value );
				}
				else if( !strcmp( label, "Sound" ) )
				{
					sound_toggle = !strcmp( value, "1" );
				}
				else if( !strcmp( label, "PacketDump" ) )
				{
					packet_toggle = !strcmp( value, "1" );
				}
				else if( !strcmp( label, "ChatFont" ) )
				{
					strcpy( ChatFontString, value );
				}
			}

			/* Color section */
			else if( !strcmp( section, "Colors" ) )
			{
				if( !strcmp( label, "Online" ) )
				{
					str2col(&color_online, value);
					color_online_set = TRUE;
				}
				if( !strcmp( label, "Offline" ) )
				{
					str2col(&color_offline, value);
					color_offline_set = TRUE;
				}
				if( !strcmp( label, "Away" ) )
				{
					str2col(&color_away, value);
					color_away_set = TRUE;
				}
				if( !strcmp( label, "Not Available" ) )
				{
					str2col(&color_na, value);
					color_na_set = TRUE;
				}
				if( !strcmp( label, "Free for Chat" ) )
				{
					str2col(&color_ffc, value);
					color_ffc_set = TRUE;
				}
				if( !strcmp( label, "Occupied" ) )
				{
					str2col(&color_occ, value);
					color_occ_set = TRUE;
				}
				if( !strcmp( label, "Do not Disturb" ) )
				{
					str2col(&color_dnd, value);
					color_dnd_set = TRUE;
				}
				if( !strcmp( label, "Invisible" ) )
				{
					str2col(&color_inv, value);
					color_inv_set = TRUE;
				}
			}

			/* Sound section added by Paul Laufer [PEL] */
			else if( !strcmp( section, "Sound"))
			{
				if(!strcmp(label, "UserOnline"))
				{
					if(!strcmp(value, "1"))
						UserOnline = TRUE;
					else
						UserOnline = FALSE;
				}
				else if(!strcmp(label, "UserOnlineSound"))
				{
					if(strlen(value) < 256)
						strcpy(UserOnlineSound, value);
				}

				else if(!strcmp(label, "UserOffline"))
				{
					if(!strcmp(value, "1"))
						UserOffline = TRUE;
					else
						UserOffline = FALSE;
				}
				else if(!strcmp(label, "UserOfflineSound"))
				{
					if(strlen(value) < 256)
						strcpy(UserOfflineSound, value);
				}

				else if(!strcmp(label, "RecvMessage"))
				{
					if(!strcmp(value, "1"))
						RecvMessage = TRUE;
					else
						RecvMessage = FALSE;
				}
				else if(!strcmp(label, "RecvMessageSound"))
				{
					if(strlen(value) < 256)
						strcpy(RecvMessageSound, value);
				}

				else if(!strcmp(label, "RecvChat"))
				{
					if(!strcmp(value, "1"))
						RecvChat = TRUE;
					else
						RecvChat = FALSE;
				}
				else if(!strcmp(label, "RecvChatSound"))
				{
					if(strlen(value) < 256)
						strcpy(RecvChatSound, value);
				}
			}
			/* End rcfile read sound config section */

			else if( !strcmp( section, "Contacts" ) )
			{
				Contact_List = TRUE;
				strip_quote( label );

				if( label[ 0 ] == '*' )
				{
					Contacts[ Num_Contacts ].invis_list = TRUE;
					Contacts[ Num_Contacts ].uin = atoi( &label[ 1 ] );
				}
				else if( label[ 0 ] == '~' )
				{
					Contacts[ Num_Contacts ].vis_list = TRUE;
					Contacts[ Num_Contacts ].uin = atoi( &label[ 1 ] );
				}
				else
				{
					Contacts[ Num_Contacts ].invis_list = FALSE;
					Contacts[ Num_Contacts ].vis_list = FALSE;
					Contacts[ Num_Contacts ].uin = atoi( label );
				}
				Contacts[ Num_Contacts ].sok = 0;
				Contacts[ Num_Contacts ].chat_sok = 0;
				Contacts[ Num_Contacts ].chat_seq = 0;
				Contacts[ Num_Contacts ].chat_active = FALSE;
				Contacts[ Num_Contacts ].chat_active2 = FALSE;
				Contacts[ Num_Contacts ].chat_port = 0;
				Contacts[ Num_Contacts ].chat_bg_red =
				Contacts[ Num_Contacts ].chat_bg_green =
				Contacts[ Num_Contacts ].chat_bg_blue = 0;
				Contacts[ Num_Contacts ].chat_fg_red =
				Contacts[ Num_Contacts ].chat_fg_green =
				Contacts[ Num_Contacts ].chat_fg_blue = 255;
				Contacts[ Num_Contacts ].log_window = gtk_window_new( GTK_WINDOW_TOPLEVEL );
				Contacts[ Num_Contacts ].tcp_gdk_input = 0;
				Contacts[ Num_Contacts ].chat_gdk_input = 0;
				Contacts[ Num_Contacts ].pdata = NULL;
				gtk_signal_connect( GTK_OBJECT( Contacts[ Num_Contacts ].log_window ), "delete_event", GTK_SIGNAL_FUNC( gtk_widget_hide_on_delete ), NULL );
				gtk_widget_set_usize( Contacts[ Num_Contacts ].log_window, 625, 250 );
				Contacts[ Num_Contacts ].log_list = gtk_text_new( NULL, NULL );
				gtk_text_set_editable( GTK_TEXT( Contacts[ Num_Contacts ].log_list ), FALSE );
				gtk_container_add( GTK_CONTAINER( Contacts[ Num_Contacts ].log_window ), Contacts[ Num_Contacts ].log_list );
				gtk_widget_show( Contacts[ Num_Contacts ].log_list );
				Contacts[ Num_Contacts ].message = NULL;
				Contacts[ Num_Contacts ].messages = 0;
				Contacts[ Num_Contacts ].status = STATUS_OFFLINE;
				Contacts[ Num_Contacts ].last_time = -1L;
				Contacts[ Num_Contacts ].current_ip = -1L;
				Contacts[ Num_Contacts ].gdk_input_tag = 0;
				memcpy( Contacts[ Num_Contacts ].nick, value, sizeof( Contacts->nick )  );
				Num_Contacts++;
			}
			fread( single, 1, 1, fp );
		}
	}
	
	fclose( fp );

	return 1;
}

int Initalize_RC_File( void )
{
#ifdef TRACE_FUNCTION
	printf( "Initialize_RC_File\n" );
#endif

	NewUserSignup();
	Create_RC_File();

	return 1;
}

int Create_RC_File( void )
{
	FILE *fp;
	char rcfile[ 256 ];
	char command[ 256 ];

#ifdef TRACE_FUNCTION
	printf( "Create_RC_File\n" );
#endif

	sprintf( rcfile, "mkdir -p %s/.icq/history", getenv( "HOME" ) );
	system( rcfile );

	sprintf( rcfile, "chmod 700 %s/.icq", getenv( "HOME" ) );
	system( rcfile );
                
	strcpy( rcfile, getenv( "HOME" ) );

	sprintf( command, "mkdir -p %s/.icq/history", rcfile );
	system( command );

	strcat( rcfile, "/.icq/gtkicqrc" );
	
	if( ( fp = fopen( rcfile, "w" ) ) == NULL )
	{
		g_print( "Error creating %s\n", rcfile );
		exit( 1 );
	}

	/* Personal Section */
	fprintf( fp, "Section \"Personal\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tUIN\t\t\"%ld\"\n", UIN );
	fprintf( fp, "\tPassword\t\"%s\"\n", passwd );
	fprintf( fp, "\tStatus\t\t\"0\"\n" );
	fprintf( fp, "\tNickname\t\"%s\"\n", nickname );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Personal\"\n\n" );

	/* Server Section */
	fprintf( fp, "Section \"Server\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tServer\t\t\"icq.mirabilis.com\"\n" );
	fprintf( fp, "\tPort\t\t\"4000\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Server\"\n\n" );

	/* Logging Section */
	/* I'll add this later, when we get a preferences screen */
	
	/* Sound Section */
	/* provided by Paul Laufer [PEL] */
	fprintf( fp, "Section \"Sound\"\n\n");
	fprintf( fp, "\tUserOnline\t\t\"1\"\n");
	fprintf( fp, "\tUserOnlineSound\t\t\"~/.icq/Online.au\"\n");
	fprintf( fp, "\tUserOffline\t\t\"0\"\n");
	fprintf( fp, "\tUserOfflineSound\t\"\"\n");
	fprintf( fp, "\tRecvMessage\t\t\"1\"\n");
	fprintf( fp, "\tRecvMessageSound\t\"~/.icq/Message.au\"\n\n");
	fprintf( fp, "\tRecvChat\t\t\"0\"\n");
	fprintf( fp, "\tRecvChatSound\t\"~/.icq/Chat.au\"\n");
	fprintf( fp, "EndSection \"Sound\"\n\n");

	/* Status Section */
	fprintf( fp, "Section \"Status\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tAWAY\t\t\"User is currently away.  Please leave a message.\"\n" );
	fprintf( fp, "\tWindowSize\t\"175x310\"\n" );
	fprintf( fp, "\tWindowTitle\t\"GtkICQ\"\n" );
	fprintf( fp, "\tSound\t\t\"0\"\n" );
	fprintf( fp, "\tPacketDump\t\"%d\"\n", FALSE );
	fprintf( fp, "\tChatFont\t\"%s\"\n", "-adobe-courier-medium-r-normal-*-*-140-*-*-m-*-iso8859-1" );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Status\"\n\n" );
	
	/* Contacts Section */
	fprintf( fp, "Section \"Contacts\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\t\"4664755\"\t\"Jeremy Wise\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Contacts\"\n" );

	fclose( fp );

	return 0;
}

/*
 *	Config File Save
 *
 *	Jordan Nelson (jordan@pathwaynet.com) 09/07/1998
 * Jeremy Wise (jwise@pathwaynet.com) 10/15/1998
 *
 */

int Save_RC( void )
{
	FILE *fp;
	char rcfile[ 256 ];
	int cx, cz, marked = 0;

#ifdef TRACE_FUNCTION
	printf( "Save_RC\n" );
#endif

	strcpy( rcfile, getenv( "HOME" ) );
	strcat( rcfile, "/.icq/gtkicqrc" );
	
	if( strlen( configfilename ) )
		strcpy( rcfile, configfilename );

	if( ( fp = fopen( rcfile, "w" ) ) == NULL )
	{
		g_print( "Error writing %s\n", rcfile );
		exit( 1 );
	}

	/* Personal Section */
	fprintf( fp, "Section \"Personal\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tUIN\t\t\"%ld\"\n", UIN );
	fprintf( fp, "\tPassword\t\"%s\"\n", passwd );
	if( Current_Status != STATUS_OFFLINE )
		fprintf( fp, "\tStatus\t\t\"%ld\"\n", Current_Status );
	else
		fprintf( fp, "\tStatus\t\t\"0\"\n" );
	fprintf( fp, "\tNickname\t\"%s\"\n", nickname );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Personal\"\n\n" );

	/* Server Section */
	fprintf( fp, "Section \"Server\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tServer\t\t\"%s\"\n", server );
	fprintf( fp, "\tPort\t\t\"%ld\"\n", remote_port );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Server\"\n\n" );

	/* Logging Section */
	/* I'll add this later, when we get a preferences screen */
	
	/* Sound Section */
	/* provided by Paul Laufer [PEL] */
	fprintf( fp, "Section \"Sound\"\n\n");
	fprintf( fp, "#Specify the full path to the sound file, no ~ allowed :(\n");
	fprintf( fp, "\tUserOnline\t\t\"%d\"\n", UserOnline);
	fprintf( fp, "\tUserOnlineSound\t\t\"%s\"\n", UserOnlineSound);
	fprintf( fp, "\tUserOffline\t\t\"%d\"\n", UserOffline);
	fprintf( fp, "\tUserOfflineSound\t\"%s\"\n", UserOfflineSound);
	fprintf( fp, "\tRecvMessage\t\t\"%d\"\n", RecvMessage);
	fprintf( fp, "\tRecvMessageSound\t\"%s\"\n", RecvMessageSound);
	fprintf( fp, "\tRecvChat\t\t\"%d\"\n", RecvChat);
	fprintf( fp, "\tRecvChatSound\t\t\"%s\"\n\n", RecvChatSound);
	fprintf( fp, "EndSection \"Sound\"\n\n");

	/* Status Section */
	fprintf( fp, "Section \"Status\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tAWAY\t\t\"%s\"\n", Away_Message );
	fprintf( fp, "\tWindowSize\t\"%dx%d\"\n", WindowWidth, WindowHeight );
	fprintf( fp, "\tWindowTitle\t\"%s\"\n", WindowTitle );
	fprintf( fp, "\tSound\t\t\"%d\"\n", sound_toggle );
	fprintf( fp, "\tPacketDump\t\"%d\"\n", packet_toggle );
	fprintf( fp, "\tChatFont\t\"%s\"\n", ChatFontString );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Status\"\n\n" );

	/* Color Section (Added by jwise) */
	fprintf( fp, "Section \"Colors\"\n" );
	fprintf( fp, "\n" );
	fprintf( fp, "\tOnline\t\t\"#%04x%04x%04x%04x\"\n",
	         color_online.red,
	         color_online.green,
	         color_online.blue,
	         (unsigned int)color_online.pixel );
	fprintf( fp, "\tOffline\t\t\"#%04x%04x%04x%04x\"\n",
	         color_offline.red,
	         color_offline.green,
	         color_offline.blue,
	         (unsigned int)color_offline.pixel );
	fprintf( fp, "\tAway\t\t\"#%04x%04x%04x%04x\"\n",
	         color_away.red,
	         color_away.green,
	         color_away.blue,
	         (unsigned int)color_away.pixel );
	fprintf( fp, "\tNot Available\t\"#%04x%04x%04x%04x\"\n",
	         color_na.red,
	         color_na.green,
	         color_na.blue,
	         (unsigned int)color_na.pixel );
	fprintf( fp, "\tFree for Chat\t\"#%04x%04x%04x%04x\"\n",
	         color_ffc.red,
	         color_ffc.green,
	         color_ffc.blue,
	         (unsigned int)color_ffc.pixel );
	fprintf( fp, "\tOccupied\t\"#%04x%04x%04x%04x\"\n",
	         color_occ.red,
	         color_occ.green,
	         color_occ.blue,
	         (unsigned int)color_occ.pixel );
	fprintf( fp, "\tDo Not Disturb\t\"#%04x%04x%04x%04x\"\n",
	         color_dnd.red,
	         color_dnd.green,
	         color_dnd.blue,
	         (unsigned int)color_dnd.pixel );
	fprintf( fp, "\tInvisible\t\"#%04x%04x%04x%04x\"\n",
	         color_inv.red,
	         color_inv.green,
	         color_inv.blue,
	         (unsigned int)color_inv.pixel );
	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Colors\"\n\n" );

	/* Contacts Section */
	fprintf( fp, "Section \"Contacts\"\n" );
	fprintf( fp, "\n" );

	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( strcmp( Contacts[ marked ].nick, Contacts[ cx ].nick ) > 0 )
			marked = cx;
		Contacts[ cx ].lb_index = 0;
	}

	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( Contacts[ marked ].uin > 0 )
			fprintf( fp, "\t\"%s%s%ld\"\t\"%s\"\n",
			         ( Contacts[ marked ].invis_list ? "*" : "" ),
						( Contacts[ marked ].vis_list ? "~" : "" ),
			         Contacts[ marked ].uin, Contacts[ marked ].nick );
		Contacts[ marked ].lb_index = 1;

		for( cz = 0; cz < Num_Contacts; cz ++ )
		{
			if( !Contacts[cz].lb_index )
			{
				marked = cz;
				break;
			}
		}

		for( cz = 0; cz < Num_Contacts; cz ++ )
		{
			if( strcmp( Contacts[ marked ].nick, Contacts[ cz ].nick ) > 0 && !Contacts[ cz ].lb_index )
				marked = cz;
		}
	}

	fprintf( fp, "\n" );
	fprintf( fp, "EndSection \"Contacts\"\n" );

	fclose( fp );

	return 0;
}

void strip_quote( char *string )
{
	int counter;
	char *replace;

	replace = ( char * ) malloc( strlen( string ) + 1 );
	
	strcpy( replace, string );
	for( counter = 0; counter < strlen( replace ); counter++ )
	{
		string[ counter ] = replace[ counter + 1 ];
	}
	string[ counter - 2 ] = '\0';

	free( replace );
}
