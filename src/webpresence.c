#include "gtkicq.h"

void icq_sendwebpresence( SOK_T sok )
{
	net_icq_pak pak;

#ifdef TRACE_FUNCTION
	printf( "icq_sendwebpresence\n" );
#endif

	Word_2_Chars( pak.head.ver, ICQ_VER );
	Word_2_Chars( pak.head.cmd, CMD_WEBPRESENCE_1 );
	Word_2_Chars( pak.head.seq, seq_num++ );
	DW_2_Chars( pak.head.UIN, UIN );

/*24*/
	pak.data[0] = Current_Status;
	pak.data[1] = 0x04;
	pak.data[2] = 0x01;
	pak.data[3] = 0x01;
	pak.data[4] = 0x01;

	packet_print( (pak.head.ver), 5 + sizeof( pak.head ) - 2,
	              PACKET_TYPE_UDP | PACKET_DIRECTION_SEND );
	SOCKWRITE( sok, &(pak.head.ver), 5 + sizeof( pak.head ) - 2);
	last_cmd[seq_num - 1 ] = Chars_2_Word( pak.head.cmd );
}

void icq_finishwebpresence( SOK_T sok )
{
	net_icq_pak pak;

#ifdef TRACE_FUNCTION
	printf( "icq_finishwebpresence\n" );
#endif

	Word_2_Chars( pak.head.ver, ICQ_VER );
	Word_2_Chars( pak.head.cmd, CMD_WEBPRESENCE_2 );
	Word_2_Chars( pak.head.seq, seq_num++ );
	DW_2_Chars( pak.head.UIN, UIN );

	pak.data[0] = 0x00;
	pak.data[1] = 0x02;
	pak.data[2] = 0x01;
	pak.data[3] = 0x00;

	packet_print( (pak.head.ver), 4 + sizeof( pak.head ) - 2,
	              PACKET_TYPE_UDP | PACKET_DIRECTION_SEND );
	SOCKWRITE( sok, &(pak.head.ver), 4 + sizeof( pak.head ) - 2);
	last_cmd[seq_num - 1 ] = Chars_2_Word( pak.head.cmd );
}
