#ifdef HAVE_CONFIG_H
	#include <config.h>
#endif

#include "gtkicq.h"
#include "pixmaps.h"

#ifdef GNOME
#include "applet.h"
#endif

int flash_messages( struct sokandlb *data )
{
	static char flash = 0;
	int cx;
	int have_message = 0;

#ifdef TRACE_FUNCTION
	printf( "flash_messages\n" );
#endif

	flash = !flash;

	if( time( NULL ) > next_resend )
		Do_Resend( data->sok );

	for( cx = 0; cx < Num_Contacts; cx ++ )
	{
		if( Contacts[ cx ].messages )
		{
			have_message = 1;
			switch( flash )
			{
				case 0:
					switch( Contacts[ cx ].message[ 0 ][ 0 ] )
					{
						case 'c':
							gtk_clist_set_pixmap( GTK_CLIST( data->lb_userwin ),
							                      Contacts[ cx ].lb_index, 0,
							                      icon_chat2_pixmap,
							                      icon_chat2_bitmap );
							break;
						default:
							gtk_clist_set_pixmap( GTK_CLIST( data->lb_userwin ),
							                      Contacts[ cx ].lb_index, 0,
							                      icon_blank_pixmap,
							                      icon_blank_bitmap );
					}
					break;
				case 1:
					switch( Contacts[ cx ].message[ 0 ][ 0 ] )
					{
						case 'm':
						default:
							gtk_clist_set_pixmap( GTK_CLIST( data->lb_userwin ),
							                      Contacts[ cx ].lb_index, 0,
							                      icon_message_pixmap,
							                      icon_message_bitmap );
							break;
						case 'c':
							gtk_clist_set_pixmap( GTK_CLIST( data->lb_userwin ),
							                      Contacts[ cx ].lb_index, 0,
							                      icon_chat_pixmap,
							                      icon_chat_bitmap );
							break;
						case 'u':
							gtk_clist_set_pixmap( GTK_CLIST( data->lb_userwin ),
							                      Contacts[ cx ].lb_index, 0,
							                      icon_url_pixmap,
							                      icon_url_bitmap );
							break;
						case 'a':
						case 'n':
							gtk_clist_set_pixmap( GTK_CLIST( data->lb_userwin ),
							                      Contacts[ cx ].lb_index, 0,
							                      icon_auth_pixmap,
							                      icon_auth_bitmap );
							 break;
					}
					
					break;
			}
		}
	}

#if 0
	if( system_messages )
	{
		switch( flash )
		{
			case 0:
				gtk_clist_set_pixmap( GTK_CLIST( data->lb_syswin ), 0, 0,
				                      icon_blank_pixmap,
				                      icon_blank_bitmap );
				break;
			case 1:
				gtk_clist_set_pixmap( GTK_CLIST( data->lb_syswin ), 0, 0,
				                      icon_message_pixmap,
				                      icon_message_bitmap );
				break;
		}
	}
#endif

#ifdef GNOME
	if( have_message )
	{
		if( flash )
			applet_update( Current_Status, icon_offline_pixmap, data );
		else
			applet_update( Current_Status, NULL, data );

	}
#endif

	return TRUE;
}
