#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <gtk/gtk.h>
#include "gtkicq.h"

void add_incoming_to_history( int uin, char *statement )
{
	char *strings[3];
	char who[32];
	time_t timedate;
	struct tm *my_tm;
	char pdate[46];
	int cx, cy, cz;
	char *halves[] = { "AM", "PM" };
	int half = 0;
	char buf[ 1024 ];

	char *filename;
	int file;

#ifdef TRACE_FUNCTION
	printf( "add_incoming_to_history\n" );
#endif

	strings[ 0 ] = pdate;
	strings[ 1 ] = who;
	strings[ 2 ] = statement;

	time( &timedate );
	my_tm = localtime( &timedate );

	if( my_tm->tm_hour > 12 )
	{
		my_tm->tm_hour -= 12 ;
		half ++;
	}

	sprintf( pdate, "\n*** %02d/%02d/%d %02d:%02d:%02d %s [ Received ] ***\n", my_tm->tm_mon + 1, my_tm->tm_mday, my_tm->tm_year, my_tm->tm_hour, my_tm->tm_min, my_tm->tm_sec, halves[ half ] );

	filename = (char *)malloc( strlen( getenv( "HOME" ) ) + 25 );
	sprintf( filename, "/%s/.icq/history/%d", getenv( "HOME" ), uin );
	if( ( file = open( filename, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR ) ) == -1 )
		return;

	free( filename );

	write( file, pdate, strlen( pdate ) );

	strcpy( buf, "" );
	cy = cz = 0;
	for( cx = 0; cx < strlen( statement ); cx ++ )
	{
		cy ++;
		if( statement[cx] == '\n' || cy == 70 ||
		    ( cy >= 60 && statement[cx] == ' ' ) )
		{
			write( file, ( statement + cz ), cy );
			if( cy >= 60 && cy != 70 )
			{
				write( file, "\n", 1 );
			}
			if( cy == 70 )
			{
				write( file, "-\n", 2 );
			}
			cz += cy;
			cy = 0;
		}
	}			

	if( cz != strlen( statement ) )
	{
		write( file, ( statement + cz ), strlen( statement ) - cz );
	}

	write( file, "\n", 1 );

	close( file );
}

void add_outgoing_to_history( int uin, char *statement )
{
	char *strings[3];
	char who[32];
	time_t timedate;
	struct tm *my_tm;
	char pdate[42];
	int cx, cy, cz;
	char *halves[] = { "AM", "PM" };
	int half = 0;
	char buf[ 1024 ];

	char *filename;
	int file;

#ifdef TRACE_FUNCTION
	printf( "add_outgoing_to_history\n" );
#endif

	strings[ 0 ] = pdate;
	strings[ 1 ] = who;
	strings[ 2 ] = statement;

	time( &timedate );
	my_tm = localtime( &timedate );

	if( my_tm->tm_hour > 12 )
	{
		my_tm->tm_hour -= 12 ;
		half ++;
	}

	sprintf( pdate, "\n*** %02d/%02d/%d %02d:%02d:%02d %s [ Sent ] ***\n", my_tm->tm_mon + 1, my_tm->tm_mday, my_tm->tm_year, my_tm->tm_hour, my_tm->tm_min, my_tm->tm_sec, halves[ half ] );

	filename = (char *)malloc( strlen( getenv( "HOME" ) ) + 25 );
	sprintf( filename, "/%s/.icq/history/%d", getenv( "HOME" ), uin );
	if( ( file = open( filename, O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR ) ) == -1 )
		return;

	free( filename );

	write( file, pdate, strlen( pdate ) );

	strcpy( buf, "" );
	cy = cz = 0;
	for( cx = 0; cx < strlen( statement ); cx ++ )
	{
		cy ++;
		if( statement[cx] == '\n' || cy == 70 ||
		    ( cy >= 60 && statement[cx] == ' ' ) )
		{
			write( file, ( statement + cz ), cy );
			if( cy >= 60 && cy != 70 )
			{
				write( file, "\n", 1 );
			}
			if( cy == 70 )
			{
				write( file, "-\n", 2 );
			}
			cz += cy;
			cy = 0;
		}
	}			

	if( cz != strlen( statement ) )
	{
		write( file, ( statement + cz ), strlen( statement ) - cz );
	}
	
	write( file, "\n", 1 );

	close( file );
}
