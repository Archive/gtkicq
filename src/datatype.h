#ifndef __MATT_DATATYPE__
#define __MATT_DATATYPE__

#include <unistd.h>

typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned char BYTE;
typedef signed long S_DWORD;
typedef signed short S_WORD;
typedef signed char S_BYTE;
typedef signed long INT32;
typedef signed short INT16;
typedef signed char INT8;
typedef unsigned long U_INT32;
typedef unsigned short U_INT16;
typedef unsigned char U_INT8;

typedef unsigned char BOOL;

#define sockread(s,p,l) read(s,p,l)
#define sockwrite(s,p,l) write(s,p,l)
#define SOCKCLOSE( s ) close(s)
#define Get_Config_Info() Get_Unix_Config_Info()
typedef int FD_T;
typedef int SOK_T;

#ifndef TRUE
	#define TRUE 1
#endif

#ifndef FALSE
	#define FALSE 0
#endif

#endif /* Matt's datatype */
