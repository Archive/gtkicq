#include "gtkicq.h"

GtkWidget *progress_indicator;

gint progress_timeout_kill( gpointer data );
gint progress_timeout (gpointer data);

gint progress_timeout_kill( gpointer data )
{
	int to_kill = GPOINTER_TO_INT( data );

#ifdef TRACE_FUNCTION
	printf( "progress_timeout_kill\n" );
#endif

	gtk_timeout_remove( to_kill );
	gtk_statusbar_pop( GTK_STATUSBAR( statusbar ),
	                   2 + to_kill );
	return FALSE;
}

gint progress_timeout (gpointer data)
{
  gfloat new_val;
  GtkAdjustment *adj;

#ifdef TRACE_FUNCTION
	printf( "progress_timeout\n" );
#endif

  adj = GTK_PROGRESS (data)->adjustment;

  new_val = adj->value + 1;
  if (new_val > adj->upper)
    new_val = adj->lower;

  gtk_progress_set_value (GTK_PROGRESS (data), new_val);

  return TRUE;
}

int show_wait( char *label_str, ProgressData *pdata )
{
#ifdef TRACE_FUNCTION
	printf( "show_wait\n" );
#endif

	if (!pdata)
		pdata = g_new0 (ProgressData, 1);

	pdata->pbar = progress_indicator;
   pdata->timer = gtk_timeout_add( 100, progress_timeout,
                                   (gpointer) pdata->pbar );
	gtk_timeout_add( 15000, progress_timeout_kill,
			 GINT_TO_POINTER( pdata->timer ) );

	return pdata->timer;
}

void progress_bar_make( void )
{
	GtkAdjustment *adj;

#ifdef TRACE_FUNCTION
	printf( "progress_bar_make\n" );
#endif

	adj = (GtkAdjustment *) gtk_adjustment_new (0, 1, 300, 0, 0, 0);

	progress_indicator = gtk_progress_bar_new_with_adjustment (adj);
	gtk_widget_set_usize( progress_indicator, 0, 20 );

	gtk_progress_set_activity_mode (GTK_PROGRESS (progress_indicator), TRUE );

	gtk_progress_bar_set_activity_step( GTK_PROGRESS_BAR( progress_indicator ),
	                                    7 );
	gtk_progress_bar_set_activity_blocks( GTK_PROGRESS_BAR( progress_indicator ),
	                                      4 );
}
