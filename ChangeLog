1998-02-14  Jeremy Wise  <jwise@pathwaynet.com>
	* More updates to applet - merged the two icons, and made it so it
		will handle vertical and horizontal panels well

1999-02-12  Tomas Ogren  <stric@ing.umu.se>

	* src/rcfile.c, src/gtkicq.h: Don't include gnome.h unless we're using
	GNOME... Added a helper-function to read the colors from gtkicqrc
	to get rid of Bus errors on some systems (%04x == int, not short).

1999-02-12  Herbert Valerio Riedel  <hvr@hvrlab.ml.org>

	* src/gtkfunc.c, src/chatdlg.c: even more 64bit cast fixes

1998-02-11  Jeremy Wise  <jwise@pathwaynet.com>
	* Fixed bug with applet.c including gnome.h for gtk-only compile

Mon Feb  8 01:17:00 1999  Timur Bakeyev <mc@bat.ru>

	* acinclude.m4: New file. If system lacks of ESD support, then 
	AM_PATH_ESD is undefined and autogen.sh fails. Hope, warning about
	double defenition of the macro more acceptable.

1999-02-04  Jeff Garzik  <jgarzik@pobox.com>

	* src/gtkfunc.c, src/response.c, src/sendmsg.c:
	Include config.h as needed.  Reported by Daniel Burrows
	<Daniel_Burrows@brown.edu>.

	* src/chatdlg.c:
	More 64-bitness casts.

1999-01-24  Jeff Garzik  <jgarzik@pobox.com>

	* src/tcp.c, src/gtkicq.c:
	Include config.h the correct way, and not only when GNOME is
	defined.
	
1999-01-23  Jeremy Wise  <jwise@pathwaynet.com>

	src/GtkICQ.gnorba, src/Makefile.am: Added GtkICQ.gnorba

	src/applet.c: Slight modifications to applet code to make double
		clicking work correctly.  Also bugs with applet were fixed.

	src/chatdlg.c, src/gtkicq.c, src/rcfile.c, src/tcp.c: Added dynamic
		fonts - can be specified now in config file.  Still needs
		some work.

	src/chatdlg.c: Chats can be logged by defining CHAT_LOG_FILE at
		compile time.  Needs a file dialog set up.

	src/gtkfunc.c: Port and IP now displayed in individual submenu

	src/rcfile.c: PacketDump option now stored in config file.

	src/response.c, src/sendmsg.c: Switched placement of last_cmd
		statements so they came before the packet is crypted.

	src/respose.c: Fixed bug so correct user is added with "Add User"
		button after a search.

	src/sendmsg.c: Packets are not sent if client is not connected to
		the server (UDP only).

	src/gtkicq.h, src/gtkicq.c, src/response.c, src/sendmsg.c,
		src/util.c: Removed compatibility with protocol v2.

1998-12-02  Jeremy Wise  <jwise@pathwaynet.com>

	src/gtkicq.h, src/response.c: Added masking for 0x80xx messages
		(mass sending)

1999-12-01  Jeremy Wise  <jwise@pathwaynet.com>

	src/rcfile.c: Fixed security issue with world-readable config files

1998-12-31  Jeremy Wise  <jwise@pathwaynet.com>

	src/gtkfunc.c, src/gtkicq.c, src/response.c, src/sendmsg.c: Fixed
		bugs with user searching and new user addition

1998-12-30  Jeremy Wise  <jwise@pathwaynet.com>

	src/gtkicq.[ch], src/sendmsg.c, src/msg_queue.[ch], src/gtkfunc.c,
	flash.c: Added resend ability - resend packets that aren't ACK'd

	tcp.c: Fixed ^G -> beep bug - too much was #ifdef'd out w/SOUND

	flash.c, gtkfunc.[ch], gtkicq.[ch], rcfile.c, response.c: Proper
		support of invisible and visible list added

	src/applet.h: Removed argp.h requirement

1998-12-29  Jeremy Wise  <jwise@pathwaynet.com>

	src/gtkfunc.c: Message window gives focus to respond widget by
		default

1998-12-27  Jeremy Wise  <jwise@pathwaynet.com>

	* src/tcp.c: Removed warning produced by getting away message

1998-12-23  Jeremy Wise  <jwise@pathwaynet.com>

	* src/applet.c: added (though commented) eventbox to fix applet
		double clicking.  The refresh isn't working right yet.

	* src/flash.c, src/gtkfunc.[ch], src/gtkicq.[ch]: '#if 0'd out the
		system message code - it's an unnecessary nuisance

	* src/gtkicq.c: Got rid of horizontal scrollbar (Finally!)

	* src/gtkicq.c: Fixed Connection History window size problems

	* src/gtkicq.[ch], src/util.c: Eliminated Get_Config_Info()

	* src/gtkconf.c: Added temporary values so values are saved ONLY
		if the user presses the "Save" button

	* src/gtkfunc.c: Status can now be changed to its current value
		(e.g. If you're online, you can set it to online)

1998-12-22  Jeff Garzik  <jgarzik@pobox.com>

	* configure.in, src/Makefile.am, src/xpms/Makefile.am:
	Fixed all bugs, temporarily disabled sound detection.

	* src/applet.h: don't include applet-lib.h

	* src/histadd.c: re-arranges includes, added string.h
	
	* src/changename.c, src/response.c, src/gtkfunc.c, src/gtkicq.c,
	src/showwait.c, src/tcp.c:
	Corrected 64-bitness warnings.  Some issues remain in gtkfunc.c.
	
	* src/ui.c:  added necessary #ifdef SOUND

1998-12-13  Kjartan Maraas  <kmaraas@fib.hl.no>

	* applet.[ch]: remove #include <applet-lib.h>
	
1998-12-04  Herbert Valerio Riedel  <hvr@hvrlab.ml.org>

	* tcp.c, showwait.c, gtkicq.c, gtkfunc.c, chatdlg.c, changename.c:
	added casting macros

-- 12/02/98 -- Eric Mitchell <ericmit@ix.netcom.com>

	* acconfig.h, configure.in, playsound.c: Added esound support.
	Configure checks for esound, and compiles in support if found.

-- 12/01/98 -- Jeff Garzik <jgarzik@pobox.com>

	* BUGS: 
	Added some.  Doesn't cover any of the UI bugs :)

	* INSTALL:
	Updated to reflect reality.

	* src/Makefile.no_gnome: removed, not needed anymore

	* src/applet.c:
	Updated for latest gnome-core.

	* src/gtkfunc.c, src/tcp.c:
	Using bzero() is not kosher.

	* configure.in, src/Makefile.am:
	Fully supports "" for GNOME compile or "--without-gnome" for
	straight Gtk+ compile.

	Removed libtool call. Thank you jamesh for autogen update!

-- 11/31/98 -- Geoff Harrison <mandrake@mandrake.net>

	* gtkicq.c - apparently someone decided that gtk_clist_set_policy
	didn't need to exist - so I took it out.

-- 11/30/98 -- Jeff Garzik <jgarzik@pobox.com>

	* configure.in, src/Makefile.am:
	Much rearrangement of configure stuff.
	Now supports --without-gnome to compile a GTK-only version.

-- 11/26/98 -- Jeff Garzik <jgarzik@pobox.com>

	* autogen.sh: Better toplevel srcdir test.
	* Remoted automake- and build-generated files COPYING,
	  mkinstalldirs, missing, install-sh, Makefile.in, libtool,
	  config.*, etc.

-- 11/26/98 -- Arturo Espinosa <arturo@nuclecu.unam.mx>

	* Changed gtk_clist_set_policy for the scrolled_window API. (GTK
          1.1.5 now required).
	* Removed a couple of warnings.

-- 00/00/00 --
	- Version 0.56 -
	* More Bug fixes
	* Added -c <config-file> command line option
	* Working GNOME applet
	* Added -a command line option (run without applet)
	* Size of window can now be changed, either smaller or bigger
	* Fixed remove user bug
	* Removed support/ directory, and no longer installing libs
	* Cannot send 0 length message
	* Added configuration window (Paul Laufer <PELaufer@CSUPomona.edu>)
	* Upgraded to V4 of the protocol (for some reason, only for Gnome)

-- 10/01/98 --
	- Version 0.55 -
	* Fixed bug with new signup routine (for new user AND existing user)
	* Fixed config.h problem for no_gnome compiling
	* Added auto-away feature (uses XScreenSaver functions)
	* Changed icon set - no longer will use Mirabilis-like icons
	* Invisible list now represents those you DO NOT want to see you
	  ( as Mirabilis clients do )

-- 09/24/98 --
	- Version 0.54 -
	* Chat beeping works
	* StatusBar and Progress indicator moved to main window
	* Moved Connection History to ICQ Menu
	* Moved status selection to the menubar
	* Changed default window size
	* Read Next button grayed out unless pending messages
	* Windows users should no longer get "Other user is using an old
	  version of ICQ" messages
	* Away messages reported in textbox instead of label
	* Retains statuses correctly when program is restarted
	* Prompts for new user signup if gtkicqrc not found
	* Offline messages are now received properly
	* URLs supported (both send and receive)
	* Shows other users your away message upon request
	* Allow other users to add your name to their list if you require
	  authorization
	* Notifies you when another user adds you to their list
	* Fixed "Add User" bug from uin search window
	* Realtime changing of invisible list
	* Window Size and Title changed on exit
	* Sorts UIN list alphabetically on exit
	* Saves sound status, so you don't have to set it every time you
	  start GtkICQ
	* Supports both hostname and n.n.n.n notation for server address

-- 09/12/98 --
	- Version 0.53 -
	* Cleaned up unnecessary defines in Makefiles
	* Chat protocol redone - works properly now
	* Away Messages now work properly
	* Closing chat works properly
	* Status windows implemented for:
		- sending TCP message
		- searching for users
		- connecting to server

-- 09/08/98 --
	- Version 0.52 -
	* Chat windows now have scrollbars
	* Add User interface works properly, Add User button added
	* Progress bar windows indicate connecting to server, sending
          messages, etc.
	* New users' UIN automatically switched to nickname in list
	* gtkicqrc file automatically saved upon close
	* Remove User works properly

-- 09/07/98 --
	- Version 0.51 -
	* Message History Cleaned up a bit
	* Cleaned up Add Contact Window, and added 'Add User' button
	* Cleaned up Rename user Window
	* Settings are now saved upon exit

	* Chat and TCP messages work
	* Colors in chat window working
	* Gnome compliant

-- 08/18/98 --
	- Version 0.50 -
	* Partial implementation of TCP code (send message works)
	* Message history works

-- 08/07/98 --
	* Icons replaced text statuses (ONLN), (AWAY), etc.

-- 07/31/98 --
	* Menu bar added, buttons removed in main window
	* Sound toggleable in the Options menu
	* Sound now plays in background (w/-DFORK_SOUND) instead of halting
	  the program

-- 07/30/98 --
	* The status string now flashes when messages are in the queue
	* Fixed seg fault bug in sound routine

-- 07/28/98 --
	* Added "Read Next" button

-- 07/27/98 --
	* Cleaned the code more
	* Fixed the problem with closing the log window
	* Added "Search / Add Users" Button to main window

-- 07/26/98 --
	* Cleaned up the code some

-- 07/25/98 --
	* Jordan Nelson (jordan@pathwaynet.com) joins the devel team
	* Users not on list are added if they send you a message
	* Changed color scheme on main user list
	* Changed ICQ Log Window to ICQ Connection History
	* Changed wording in Connection History
	* Completly dumped logging to micq_log

-- 07/23/98 --
	* Rearranged layout of log window
	* User Info works properly
	* User Information saved on exit
	* Change User works properly

-- 07/20/98 --
	* Fixed yet another segv bug in the 071998 version
	* Added ability for invisible list (thanks Matt)

-- 07/19/98 --
	* Seperate log window
	* Partial code written for personal history section

-- 07/15/98 --
	* Fixed very annoying bug that caused constant SIGSEGVs

-- 07/14/98 --
	* Added the log window which shows events as they occur

-- 07/11/98 --
	* Now works fine in < 24bpp X displays
	* Right clicking brings up a menu (not very functional, yet)

-- 07/10/98 --
	* Removed bug from "incoming message" that caused the segv :-)

-- 07/09/98 --
	* Added buffering of messages (using -DQUEUE_MESSAGES in Makefile)

-- 07/08/98 --
	* Allowed the optional compiling of colors (using -DUSE_COLOR
	  in Makefile)

-- 07/05/98 --
	* Added colored names for the list
	* Bug fixes
	* Changed GtkList to GtkCList
