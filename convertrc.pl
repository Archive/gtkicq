#!/usr/bin/perl

#
# mICQ -> GTK-ICQ Config Converter
#
# Jordan Nelson ( jordan@pathwaynet.com ) 08/08/1998
#

if( !$ARGV[ 0 ] )
{
	print "Usage: $0 <file>\n";
	print "\n";
	print "\t<file>\t\tmICQ Configuration File\n";
	print "\n";
	print "  mICQ -> GTK-ICQ Config Converter will take\n";
	print "  your original mICQ config file and convert it\n";
	print "  to the format used by GTK-ICQ, and dump to the file\n";
	print "  `~/.icq/gtkicqrc'.\n";
	die "\n";	
}

chop( $date = `date +%Y%m%d%l%M%S%p` );

$date =~ /(....)(..)(..)(..)(..)(..)(..)/;

$year   = $1;
$month  = $2;
$day    = $3;
$hour   = $4;
$minute = $5;
$second = $6;
$tod    = $7;

$num      = 0;
$contacts = 0;

open( MICQ, $ARGV[ 0 ] ) || die "Cannot Open $ARGV[ 0 ]\n";
open( GTKICQ, ">$ENV{HOME}/.icq/gtkicqrc" ) || die "Cannot Open $ENV{HOME}/.icq/gtkicqrc\n";

while( <MICQ> )
{
	$line = $_;
	chop( $_ );
	( $label, $value ) = split( / +/, $_ );

	if( $contacts )
	{
		$user[ $num ][ 0 ] = $label;
		$user[ $num ][ 1 ] = $value;
		$num++;
	}

	if( $label eq "UIN" )
	{
		$uin = $value;
	}
	elsif( $label eq "Password" )
	{
		$password = $value;
	}
	elsif( $label eq "Status" )
	{
		$status = $value;
	}
	elsif( $label eq "Server" )
	{
		$server = $value;
	}
	elsif( $label eq "Port" )
	{
		$port = $value;
	}
	elsif( $label eq "auto_rep_str_away" )
	{
		$away = substr( $line, length( "auto_rep_str_away" ) + 1, -1 );
	}
	elsif( $label eq "auto_rep_str_na" )
	{
		$na = substr( $line, length( "auto_rep_str_na" ) + 1, -1 );
	}
	elsif( $label eq "auto_rep_str_dnd" )
	{
		$dnd = substr( $line, length( "auto_rep_str_dnd" ) + 1, -1 );
	}
	elsif( $label eq "auto_rep_str_occ" )
	{
		$occ = substr( $line, length( "auto_rep_str_occ" ) + 1, -1 );
	}
	elsif( $label eq "auto_rep_str_inv" )
	{
		$inv = substr( $line, length( "auto_rep_str_inv" ) + 1, -1 );
	}
	elsif( $label eq "Contacts" )
	{
		$contacts = 1;
	}
}

close( MICQ );

print GTKICQ "#\n";
print GTKICQ "# GTK-ICQ Configuration File\n";
print GTKICQ "#\n";
print GTKICQ "# Generated by mICQ -> GTK-ICQ Converter\n";
print GTKICQ "# $month/$day/$year $hour:$minute:$second $tod\n";
print GTKICQ "#\n";
print GTKICQ "\n";

print GTKICQ "Section \"Personal\"\n";
print GTKICQ "\n";
print GTKICQ "\tUIN\t\t\"$uin\"\n";
print GTKICQ "\tPassword\t\"$password\"\n";
print GTKICQ "\tStatus\t\t\"$status\"\n";
print GTKICQ "\tNickname\t\"$uin\"\n";
print GTKICQ "\n";
print GTKICQ "EndSection \"Personal\"\n";
print GTKICQ "\n";

print GTKICQ "Section \"Server\"\n";
print GTKICQ "\n";
print GTKICQ "\tServer\t\t\"$server\"\n";
print GTKICQ "\tPort\t\t\"$port\"\n";
print GTKICQ "\n";
print GTKICQ "EndSection \"Server\"\n";
print GTKICQ "\n";

print GTKICQ "Section \"Logging\"\n";
print GTKICQ "\n";
print GTKICQ "\tMaster\t\t\"On\"\n";
print GTKICQ "\tUserEvents\t\"On\"\n";
print GTKICQ "\tLogin\t\t\"On\"\n";
print GTKICQ "\tLogout\t\t\"On\"\n";
print GTKICQ "\tModeChange\t\"On\"\n";
print GTKICQ "\tSentMessage\t\"Off\"\n";
print GTKICQ "\tReceiveMessage\t\"Off\"\n";
print GTKICQ "\n";
print GTKICQ "EndSection \"Logging\"\n";
print GTKICQ "\n";

print GTKICQ "Section \"Sound\"\n";
print GTKICQ "\n";
print GTKICQ "\tMaster\t\t\t\"On\"\n";
print GTKICQ "\tUserOnline\t\t\"On\"\n";
print GTKICQ "\tUserOnlineSound\t\t\"Online.au\"\n";
print GTKICQ "\tUserOffline\t\t\"Off\"\n";
print GTKICQ "\tRecvMessage\t\t\"On\"\n";
print GTKICQ "\tRecvMessageSound\t\"Message.au\"\n";
print GTKICQ "\tSentMessage\t\t\"Off\"\n";
print GTKICQ "\n";
print GTKICQ "EndSection \"Sound\"\n";
print GTKICQ "\n";

print GTKICQ "Section \"Status\"\n";
print GTKICQ "\n";
print GTKICQ "\tAWY\t\t\"$away\"\n";
print GTKICQ "\tNA\t\t\"$na\"\n";
print GTKICQ "\tDND\t\t\"$dnd\"\n";
print GTKICQ "\tOCC\t\t\"$occ\"\n";
print GTKICQ "\tINV\t\t\"$inv\"\n";
print GTKICQ "\n";
print GTKICQ "EndSection \"Status\"\n";
print GTKICQ "\n";

print GTKICQ "Section \"Contacts\"\n";
print GTKICQ "\n";
for( $i = 0; $i < $num; $i++ )
{
	print GTKICQ "\t\"$user[ $i ][ 0 ]\"\t\"$user[ $i ][ 1 ]\"\n";
}
print GTKICQ "\n";
print GTKICQ "EndSection \"Contacts\"\n";

close( GTKICQ );
